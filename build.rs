use std::{env, path::Path, process::Command};

include!("src/config.rs");

const SOURCE_DIR: &str = "data/resources";

fn main() {
    compile_resources_width_deps_check("icons.gresource");
    compile_resources_width_deps_check("styles.gresource");

    let appdata_path = "data/io.gitlab.news_flash.NewsFlash.appdata.xml.in.in";
    let appdata = std::fs::read_to_string(appdata_path).unwrap();
    let appdata = appdata.replace("@appid@", APP_ID);
    std::fs::write("data/resources/io.gitlab.news_flash.NewsFlash.appdata.xml", appdata).unwrap();

    println!("cargo:rerun-if-changed={appdata_path}");
    compile_resources("appdata.gresource");

    std::fs::remove_file("data/resources/io.gitlab.news_flash.NewsFlash.appdata.xml").unwrap();
}

fn compile_resources_width_deps_check(gresource: &str) {
    for dependency in resource_dependencies(gresource).split_whitespace() {
        println!("cargo:rerun-if-changed={dependency}");
    }
    compile_resources(gresource)
}

fn compile_resources(gresource: &str) {
    let xml_file = format!("{SOURCE_DIR}/{gresource}.xml");
    println!("cargo:rerun-if-changed={xml_file}");

    let out_dir = env::var("OUT_DIR").unwrap();
    let out_dir = Path::new(&out_dir);

    let output = Command::new("glib-compile-resources")
        .arg("--sourcedir")
        .arg(SOURCE_DIR)
        .arg("--target")
        .arg(out_dir.join(gresource))
        .arg(xml_file)
        .output()
        .unwrap();

    assert!(
        output.status.success(),
        "glib-compile-resources failed with exit status {} and stderr:\n{}",
        output.status,
        String::from_utf8_lossy(&output.stderr)
    );
}

fn resource_dependencies(gresource: &str) -> String {
    let output = Command::new("glib-compile-resources")
        .arg("--sourcedir")
        .arg(SOURCE_DIR)
        .arg("--generate-dependencies")
        .arg(format!("{SOURCE_DIR}/{gresource}.xml"))
        .output()
        .unwrap()
        .stdout;
    String::from_utf8(output).unwrap()
}
