#![allow(clippy::derived_hash_with_manual_eq)]

mod about_dialog;
mod account_popover;
mod account_widget;
mod add_dialog;
mod app;
mod article_list;
mod article_view;
mod color;
mod config;
mod content_page;
mod discover;
mod edit_category_dialog;
mod edit_feed_dialog;
mod edit_tag_dialog;
mod enclosure_button;
mod enclosure_popover;
mod error;
mod error_dialog;
mod faviconcache;
mod i18n;
mod image_dialog;
mod login_screen;
mod main_window;
mod main_window_actions;
mod reset_page;
mod responsive;
mod self_stack;
mod settings;
mod share;
mod shortcuts_dialog;
mod sidebar;
mod tag_popover;
mod undo_delete_action;
mod undo_mark_read_action;
mod util;
mod video_dialog;
mod welcome_screen;

use crate::app::App;
use crate::config::APP_ID;
use gettextrs::*;
use gtk4::prelude::*;
use log::LevelFilter;
use log4rs::append::console::ConsoleAppender;
use log4rs::config::{Appender, Config, Logger, Root};
use log4rs::encode::pattern::PatternEncoder;

fn main() {
    // nicer backtrace
    color_backtrace::install();

    let log_level = if let Ok(rust_log) = std::env::var("RUST_LOG") {
        match rust_log.to_lowercase().as_str() {
            "debug" => LevelFilter::Debug,
            _ => LevelFilter::Info,
        }
    } else {
        LevelFilter::Info
    };

    // Logging
    let encoder = PatternEncoder::new("{d(%H:%M:%S)} - {h({({l}):5.5})} - {m:<35.} (({M}:{L}))\n");
    let stdout = ConsoleAppender::builder().encoder(Box::new(encoder)).build();
    let appender = Appender::builder().build("stdout", Box::new(stdout));
    let root = Root::builder().appender("stdout").build(log_level);

    let r2d2 = Logger::builder().additive(false).build("r2d2", LevelFilter::Off);

    let config = Config::builder()
        .appender(appender)
        .logger(r2d2)
        .build(root)
        .expect("Failed to create log4rs config.");
    let _handle = log4rs::init_config(config).expect("Failed to init log4rs config.");

    // Gtk setup
    gtk4::init().expect("Error initializing gtk.");
    gtk4::Window::set_default_icon_name(APP_ID);
    glib::set_application_name("Newsflash");
    glib::set_prgname(Some("news_flash"));

    glib::setenv("CLAPPER_USE_PLAYBIN3", "1", false).unwrap();
    clapper::init().expect("Failed to initialize clapper");

    // Setup translations
    setlocale(LocaleCategory::LcAll, "");
    let localedir = bindtextdomain(config::GETTEXT_PACKAGE, config::LOCALEDIR).expect("Unable to bind the text domain");
    let textdomain = textdomain(config::GETTEXT_PACKAGE).expect("Unable to switch to the text domain");

    log::debug!("LOCALEDIR: {:?}", localedir);
    if let Ok(textdomain) = std::str::from_utf8(&textdomain) {
        log::debug!("TEXTDOMAIN: {}", textdomain);
    }

    // Run app itself
    App::new().run();
}
