use crate::{app::App, color::ColorRGBA, util::constants};
use gdk4::RGBA;
use glib::{clone, subclass};
use gtk4::{prelude::*, subclass::prelude::*, ColorDialogButton, CompositeTemplate, Widget};
use libadwaita::{prelude::*, subclass::prelude::*, Dialog, EntryRow};
use news_flash::models::Tag;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/edit_dialogs/tag.blp")]
    pub struct EditTagDialog {
        #[template_child]
        pub tag_name: TemplateChild<EntryRow>,
        #[template_child]
        pub color_button: TemplateChild<ColorDialogButton>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for EditTagDialog {
        const NAME: &'static str = "EditTagDialog";
        type Type = super::EditTagDialog;
        type ParentType = Dialog;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for EditTagDialog {}

    impl WidgetImpl for EditTagDialog {}

    impl AdwDialogImpl for EditTagDialog {}
}

glib::wrapper! {
    pub struct EditTagDialog(ObjectSubclass<imp::EditTagDialog>)
        @extends Widget, Dialog;
}

impl EditTagDialog {
    pub fn new(tag: Tag) -> Self {
        let dialog: EditTagDialog = glib::Object::new();
        let imp = dialog.imp();

        imp.tag_name.set_text(&tag.label);
        imp.tag_name.connect_apply(clone!(
            #[strong]
            tag,
            #[weak]
            dialog,
            #[upgrade_or_panic]
            move |entry| {
                let imp = dialog.imp();
                let rgba = imp.color_button.rgba();
                let color = ColorRGBA::from_normalized(
                    rgba.red() as f64,
                    rgba.green() as f64,
                    rgba.blue() as f64,
                    rgba.alpha() as f64,
                );

                App::default().edit_tag(&tag, entry.text().as_str(), &Some(color.as_string_no_alpha()));
            }
        ));

        let color = tag.color.as_deref().unwrap_or(constants::TAG_DEFAULT_COLOR);
        if let Ok(rgba) = ColorRGBA::parse_string(color) {
            imp.color_button.set_rgba(&RGBA::new(
                rgba.red_normalized() as f32,
                rgba.green_normalized() as f32,
                rgba.blue_normalized() as f32,
                rgba.alpha_normalized() as f32,
            ));
        }

        imp.color_button.connect_rgba_notify(clone!(
            #[weak]
            dialog,
            #[upgrade_or_panic]
            move |color_button| {
                let imp = dialog.imp();
                let rgba = color_button.rgba();
                let color = ColorRGBA::from_normalized(
                    rgba.red() as f64,
                    rgba.green() as f64,
                    rgba.blue() as f64,
                    rgba.alpha() as f64,
                );

                App::default().edit_tag(&tag, imp.tag_name.text().as_str(), &Some(color.as_string_no_alpha()));
            }
        ));

        dialog
    }
}
