use crate::app::App;
use crate::color::ColorRGBA;
use glib::clone;
use gtk4::{prelude::*, subclass::prelude::*, Button, CompositeTemplate, Widget};
use libadwaita::{prelude::*, Dialog, EntryRow};

mod imp {
    use super::*;
    use glib::subclass;
    use libadwaita::subclass::dialog::AdwDialogImpl;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/add_dialog/tag.blp")]
    pub struct AddTagDialog {
        #[template_child]
        pub add_tag_button: TemplateChild<Button>,
        #[template_child]
        pub tag_entry: TemplateChild<EntryRow>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AddTagDialog {
        const NAME: &'static str = "AddTagDialog";
        type ParentType = Dialog;
        type Type = super::AddTagDialog;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for AddTagDialog {}

    impl WidgetImpl for AddTagDialog {}

    impl AdwDialogImpl for AddTagDialog {}
}

glib::wrapper! {
    pub struct AddTagDialog(ObjectSubclass<imp::AddTagDialog>)
        @extends Widget, Dialog;
}

impl Default for AddTagDialog {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl AddTagDialog {
    pub fn new() -> Self {
        let dialog = Self::default();
        dialog.init();
        dialog
    }

    fn init(&self) {
        let imp = self.imp();

        let tag_add_button = imp.add_tag_button.get();
        let tag_entry = imp.tag_entry.get();

        // make parse button sensitive if entry contains text and vice versa
        imp.tag_entry.connect_changed(clone!(
            #[weak]
            tag_add_button,
            #[upgrade_or_panic]
            move |entry| {
                tag_add_button.set_sensitive(!entry.text().as_str().is_empty());
            }
        ));

        // hit enter in entry to add tag
        imp.tag_entry.connect_entry_activated(clone!(
            #[weak]
            tag_add_button,
            #[upgrade_or_panic]
            move |_entry| {
                if tag_add_button.get_sensitive() {
                    tag_add_button.emit_clicked();
                }
            }
        ));

        imp.add_tag_button.connect_clicked(clone!(
            #[weak(rename_to = widget)]
            self,
            #[weak]
            tag_entry,
            #[upgrade_or_panic]
            move |_button| {
                let rgba = ColorRGBA::new(25, 220, 114, 1);
                let color = rgba.as_string_no_alpha();
                if !tag_entry.text().as_str().is_empty() {
                    App::default().add_tag(color, tag_entry.text().as_str().into(), None);
                    widget.close();
                }
            }
        ));
    }
}
