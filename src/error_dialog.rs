use crate::app::App;
use crate::error::NewsFlashGtkError;
use glib::subclass;
use gtk4::{prelude::*, subclass::prelude::*, Button, CompositeTemplate, Widget};
use gtk4::{TextBuffer, TextView};
use libadwaita::{prelude::*, subclass::prelude::*, ActionRow, Dialog, ExpanderRow, PreferencesGroup};
use news_flash::error::{DatabaseError, FavIconError, FeedApiError, FeedParserError, NewsFlashError, OpmlError};
use news_flash::feed_api::portal::PortalError;
use serde_json::Value;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/error_detail_dialog.blp")]
    pub struct ErrorDialog {
        #[template_child]
        pub group: TemplateChild<PreferencesGroup>,
        #[template_child]
        pub clipboard_button: TemplateChild<Button>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ErrorDialog {
        const NAME: &'static str = "ErrorDialog";
        type Type = super::ErrorDialog;
        type ParentType = Dialog;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ErrorDialog {}

    impl WidgetImpl for ErrorDialog {}

    impl AdwDialogImpl for ErrorDialog {}
}

glib::wrapper! {
    pub struct ErrorDialog(ObjectSubclass<imp::ErrorDialog>)
        @extends Widget, Dialog;
}

impl ErrorDialog {
    pub fn new(error: &NewsFlashGtkError) -> Self {
        let obj: ErrorDialog = glib::Object::new();
        obj.set_error(error);
        obj
    }

    fn set_error(&self, error: &NewsFlashGtkError) {
        let error_report = format!("{:#?}", error);
        self.imp().clipboard_button.connect_clicked(move |_button| {
            App::default().main_window().clipboard().set_text(&error_report);
        });
        match error {
            NewsFlashGtkError::NewsFlash { source, context } => {
                self.add_row(Some(0), context);
                self.add_row(Some(1), &source.to_string());

                match source {
                    NewsFlashError::NotLoggedIn
                    | NewsFlashError::Thumbnail
                    | NewsFlashError::GrabContent
                    | NewsFlashError::Syncing
                    | NewsFlashError::Semaphore(_)
                    | NewsFlashError::Unknown
                    | NewsFlashError::LoadBackend
                    | NewsFlashError::Offline => {}

                    NewsFlashError::Url(e) => {
                        self.add_row(Some(2), &e.to_string());
                    }

                    NewsFlashError::ImageDownload(e) => {
                        self.add_row(Some(2), &e.to_string());
                    }

                    NewsFlashError::Database(e) => {
                        self.add_row(Some(2), &e.to_string());
                        match e {
                            DatabaseError::Open | DatabaseError::InvalidPath | DatabaseError::Unknown => {}

                            DatabaseError::Migration => self.add_row(Some(3), "Migration: check available disk space"),
                            DatabaseError::Query(q) => self.add_row(Some(3), &q.to_string()),
                            DatabaseError::Pool(p) => self.add_row(Some(3), &p.to_string()),
                            DatabaseError::IO(io) => self.add_row(Some(3), &io.to_string()),
                        }
                    }
                    NewsFlashError::API(e) => {
                        self.add_row(Some(2), &e.to_string());
                        match e {
                            FeedApiError::Auth
                            | FeedApiError::ApiLimit
                            | FeedApiError::Semaphore(_)
                            | FeedApiError::Login
                            | FeedApiError::Resource
                            | FeedApiError::Unsupported
                            | FeedApiError::Encryption
                            | FeedApiError::Unknown => {}

                            FeedApiError::UnsupportedVersion { min_supported, found } => {
                                let found = if let Some(found) = found {
                                    format!(", but found '{}'", found)
                                } else {
                                    String::new()
                                };
                                self.add_row(
                                    Some(3),
                                    &format!("Minimum version requirement '{}'{}", min_supported, found),
                                )
                            }
                            FeedApiError::Config(config_error) => self.add_row(Some(3), &config_error.to_string()),
                            FeedApiError::Url(url_error) => self.add_row(Some(3), &url_error.to_string()),
                            FeedApiError::Json { source, json } => {
                                self.add_row(Some(3), &source.to_string());
                                self.add_json_data(json);
                            }
                            FeedApiError::Network(network_error) => self.add_row(Some(3), &network_error.to_string()),
                            FeedApiError::Portal(portal_error) => {
                                self.add_row(Some(3), &portal_error.to_string());
                                match portal_error {
                                    PortalError::Unknown => {}
                                    PortalError::DB(db_error) => self.add_row(Some(4), &db_error.to_string()),
                                }
                            }
                            FeedApiError::IO(io_error) => self.add_row(Some(3), &io_error.to_string()),
                            FeedApiError::Api { message } => self.add_row(Some(3), message),
                            FeedApiError::ParseFeed(e) => {
                                self.add_row(Some(3), &e.to_string());
                                match e {
                                    FeedParserError::Html
                                    | FeedParserError::Feed
                                    | FeedParserError::NoUrl
                                    | FeedParserError::Semaphore(_) => {}
                                    FeedParserError::Http(http_error) => self.add_row(Some(3), &http_error.to_string()),
                                }
                            }
                        }
                    }
                    NewsFlashError::IO(io_error) => self.add_row(Some(2), &io_error.to_string()),
                    NewsFlashError::Icon(e) => {
                        self.add_row(Some(2), &e.to_string());
                        match e {
                            FavIconError::Html
                            | FavIconError::NoFeed
                            | FavIconError::Resize
                            | FavIconError::Semaphore(_) => {}
                            FavIconError::DB(db_error) => self.add_row(Some(3), &db_error.to_string()),
                            FavIconError::Http(http_error) => self.add_row(Some(3), &http_error.to_string()),
                        }
                    }
                    NewsFlashError::OPML(e) => {
                        self.add_row(Some(2), &e.to_string());
                        match e {
                            OpmlError::BodyHasNoOutlines => {}
                            OpmlError::IoError(io_error) => self.add_row(Some(3), &io_error.to_string()),
                            OpmlError::UnsupportedVersion(ver_error) => self.add_row(Some(3), &ver_error.to_string()),
                            OpmlError::XmlError(xml_error) => self.add_row(Some(3), &xml_error.to_string()),
                        }
                    }
                }
            }
            NewsFlashGtkError::Other(_error) => {
                // FIXME
            }
        }
    }

    fn add_row(&self, lvl: Option<u32>, text: &str) {
        let lvl = if let Some(lvl) = lvl {
            format!("<span foreground=\"gray\">({})</span> ", lvl)
        } else {
            "".into()
        };
        let row = ActionRow::builder().title(format!("{}{}", lvl, text)).build();
        self.imp().group.add(&row);
    }

    fn add_additional_data(&self, title: &str, data: &str) {
        let row = ExpanderRow::builder().title(title).build();
        let buffer = TextBuffer::builder().text(data).build();
        let textview = TextView::builder()
            .buffer(&buffer)
            .margin_top(5)
            .margin_bottom(5)
            .margin_start(5)
            .margin_end(5)
            .top_margin(5)
            .bottom_margin(5)
            .left_margin(5)
            .right_margin(5)
            .build();
        textview.add_css_class("error-data");
        row.add_row(&textview);
        self.imp().group.add(&row);
    }

    fn add_json_data(&self, json: &str) {
        let pretty_json = serde_json::from_str::<Value>(json)
            .ok()
            .and_then(|json_value| serde_json::to_string_pretty(&json_value).ok());

        self.add_additional_data(
            "JSON Data",
            if let Some(pretty_json) = pretty_json.as_deref() {
                pretty_json
            } else {
                json
            },
        )
    }
}
