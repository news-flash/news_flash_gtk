mod app_page;
mod keybinding_editor;
mod share_page;
mod shortcuts_page;
mod theme_chooser;
mod views_page;

use self::{
    app_page::SettingsAppPage, share_page::SettingsSharePage, shortcuts_page::SettingsShortcutsPage,
    views_page::SettingsViewsPage,
};

use glib::subclass;
use gtk4::{CompositeTemplate, Widget};
use libadwaita::{subclass::prelude::*, Dialog, PreferencesDialog};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/settings/dialog.blp")]
    pub struct SettingsDialog {
        #[template_child]
        pub views_page: TemplateChild<SettingsViewsPage>,
        #[template_child]
        pub app_page: TemplateChild<SettingsAppPage>,
        #[template_child]
        pub shortcuts_page: TemplateChild<SettingsShortcutsPage>,
        #[template_child]
        pub share_page: TemplateChild<SettingsSharePage>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SettingsDialog {
        const NAME: &'static str = "SettingsDialog";
        type ParentType = PreferencesDialog;
        type Type = super::SettingsDialog;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SettingsDialog {}

    impl WidgetImpl for SettingsDialog {}

    impl AdwDialogImpl for SettingsDialog {}

    impl PreferencesDialogImpl for SettingsDialog {}
}

glib::wrapper! {
    pub struct SettingsDialog(ObjectSubclass<imp::SettingsDialog>)
        @extends Widget, Dialog, PreferencesDialog;
}

impl Default for SettingsDialog {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl SettingsDialog {
    pub fn new() -> Self {
        Self::default()
    }
}
