use crate::app::App;
use crate::i18n::i18n;
use crate::settings::dialog::keybinding_editor::{KeybindState, KeybindingEditor};
use crate::settings::keybindings::Keybindings;
use glib::clone;
use gtk4::{prelude::*, subclass::prelude::*, CompositeTemplate, Label, Widget};
use libadwaita::{prelude::*, subclass::prelude::*, ActionRow, PreferencesPage};

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/settings/shortcuts.blp")]
    pub struct SettingsShortcutsPage {
        #[template_child]
        pub next_article_row: TemplateChild<ActionRow>,
        #[template_child]
        pub next_article_label: TemplateChild<Label>,

        #[template_child]
        pub previous_article_row: TemplateChild<ActionRow>,
        #[template_child]
        pub previous_article_label: TemplateChild<Label>,

        #[template_child]
        pub toggle_read_row: TemplateChild<ActionRow>,
        #[template_child]
        pub toggle_read_label: TemplateChild<Label>,

        #[template_child]
        pub toggle_marked_row: TemplateChild<ActionRow>,
        #[template_child]
        pub toggle_marked_label: TemplateChild<Label>,

        #[template_child]
        pub open_browser_row: TemplateChild<ActionRow>,
        #[template_child]
        pub open_browser_label: TemplateChild<Label>,

        #[template_child]
        pub copy_url_row: TemplateChild<ActionRow>,
        #[template_child]
        pub copy_url_label: TemplateChild<Label>,

        #[template_child]
        pub next_item_row: TemplateChild<ActionRow>,
        #[template_child]
        pub next_item_label: TemplateChild<Label>,

        #[template_child]
        pub previous_item_row: TemplateChild<ActionRow>,
        #[template_child]
        pub previous_item_label: TemplateChild<Label>,

        #[template_child]
        pub toggle_category_expanded_row: TemplateChild<ActionRow>,
        #[template_child]
        pub toggle_category_expanded_label: TemplateChild<Label>,

        #[template_child]
        pub sidebar_set_read_row: TemplateChild<ActionRow>,
        #[template_child]
        pub sidebar_set_read_label: TemplateChild<Label>,

        #[template_child]
        pub scroll_up_row: TemplateChild<ActionRow>,
        #[template_child]
        pub scroll_up_label: TemplateChild<Label>,

        #[template_child]
        pub scroll_down_row: TemplateChild<ActionRow>,
        #[template_child]
        pub scroll_down_label: TemplateChild<Label>,

        #[template_child]
        pub scrap_content_row: TemplateChild<ActionRow>,
        #[template_child]
        pub scrap_content_label: TemplateChild<Label>,

        #[template_child]
        pub tag_row: TemplateChild<ActionRow>,
        #[template_child]
        pub tag_label: TemplateChild<Label>,

        #[template_child]
        pub fullscreen_row: TemplateChild<ActionRow>,
        #[template_child]
        pub fullscreen_label: TemplateChild<Label>,

        #[template_child]
        pub close_row: TemplateChild<ActionRow>,
        #[template_child]
        pub close_label: TemplateChild<Label>,

        #[template_child]
        pub search_row: TemplateChild<ActionRow>,
        #[template_child]
        pub search_label: TemplateChild<Label>,

        #[template_child]
        pub refresh_row: TemplateChild<ActionRow>,
        #[template_child]
        pub refresh_label: TemplateChild<Label>,

        #[template_child]
        pub quit_row: TemplateChild<ActionRow>,
        #[template_child]
        pub quit_label: TemplateChild<Label>,

        #[template_child]
        pub all_articles_row: TemplateChild<ActionRow>,
        #[template_child]
        pub all_articles_label: TemplateChild<Label>,

        #[template_child]
        pub only_unread_row: TemplateChild<ActionRow>,
        #[template_child]
        pub only_unread_label: TemplateChild<Label>,

        #[template_child]
        pub only_starred_row: TemplateChild<ActionRow>,
        #[template_child]
        pub only_starred_label: TemplateChild<Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SettingsShortcutsPage {
        const NAME: &'static str = "SettingsShortcutsPage";
        type ParentType = PreferencesPage;
        type Type = super::SettingsShortcutsPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SettingsShortcutsPage {
        fn constructed(&self) {
            self.obj().setup_interaction();
        }
    }

    impl WidgetImpl for SettingsShortcutsPage {}

    impl PreferencesPageImpl for SettingsShortcutsPage {}
}

glib::wrapper! {
    pub struct SettingsShortcutsPage(ObjectSubclass<imp::SettingsShortcutsPage>)
        @extends Widget, PreferencesPage;
}

impl Default for SettingsShortcutsPage {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl SettingsShortcutsPage {
    pub fn new() -> Self {
        Self::default()
    }

    fn setup_interaction(&self) {
        let imp = self.imp();
        let settings = App::default().settings();

        self.setup_keybinding_row(
            &imp.next_article_row,
            &imp.next_article_label,
            "next_article",
            settings.read().get_keybind_article_list_next(),
        );
        self.setup_keybinding_row(
            &imp.previous_article_row,
            &imp.previous_article_label,
            "previous_article",
            settings.read().get_keybind_article_list_prev(),
        );
        self.setup_keybinding_row(
            &imp.toggle_read_row,
            &imp.toggle_read_label,
            "toggle_read",
            settings.read().get_keybind_article_list_read(),
        );
        self.setup_keybinding_row(
            &imp.toggle_marked_row,
            &imp.toggle_marked_label,
            "toggle_marked",
            settings.read().get_keybind_article_list_mark(),
        );
        self.setup_keybinding_row(
            &imp.open_browser_row,
            &imp.open_browser_label,
            "open_browser",
            settings.read().get_keybind_article_list_open(),
        );
        self.setup_keybinding_row(
            &imp.copy_url_row,
            &imp.copy_url_label,
            "copy_url",
            settings.read().get_keybind_article_list_copy_url(),
        );

        self.setup_keybinding_row(
            &imp.next_item_row,
            &imp.next_item_label,
            "next_item",
            settings.read().get_keybind_feed_list_next(),
        );
        self.setup_keybinding_row(
            &imp.previous_item_row,
            &imp.previous_item_label,
            "previous_item",
            settings.read().get_keybind_feed_list_prev(),
        );
        self.setup_keybinding_row(
            &imp.toggle_category_expanded_row,
            &imp.toggle_category_expanded_label,
            "toggle_category_expanded",
            settings.read().get_keybind_feed_list_toggle_expanded(),
        );
        self.setup_keybinding_row(
            &imp.sidebar_set_read_row,
            &imp.sidebar_set_read_label,
            "sidebar_set_read",
            settings.read().get_keybind_sidebar_set_read(),
        );

        self.setup_keybinding_row(
            &imp.refresh_row,
            &imp.refresh_label,
            "refresh",
            settings.read().get_keybind_refresh(),
        );
        self.setup_keybinding_row(
            &imp.search_row,
            &imp.search_label,
            "search",
            settings.read().get_keybind_search(),
        );
        self.setup_keybinding_row(
            &imp.quit_row,
            &imp.quit_label,
            "quit",
            settings.read().get_keybind_quit(),
        );
        self.setup_keybinding_row(
            &imp.all_articles_row,
            &imp.all_articles_label,
            "all_articles",
            settings.read().get_keybind_all_articles(),
        );
        self.setup_keybinding_row(
            &imp.only_unread_row,
            &imp.only_unread_label,
            "only_unread",
            settings.read().get_keybind_only_unread(),
        );
        self.setup_keybinding_row(
            &imp.only_starred_row,
            &imp.only_starred_label,
            "only_starred",
            settings.read().get_keybind_only_starred(),
        );

        self.setup_keybinding_row(
            &imp.scroll_up_row,
            &imp.scroll_up_label,
            "scroll_up",
            settings.read().get_keybind_article_view_up(),
        );
        self.setup_keybinding_row(
            &imp.scroll_down_row,
            &imp.scroll_down_label,
            "scroll_down",
            settings.read().get_keybind_article_view_down(),
        );
        self.setup_keybinding_row(
            &imp.scrap_content_row,
            &imp.scrap_content_label,
            "scrap_content",
            settings.read().get_keybind_article_view_scrap(),
        );
        self.setup_keybinding_row(
            &imp.tag_row,
            &imp.tag_label,
            "tag",
            settings.read().get_keybind_article_view_tag(),
        );
        self.setup_keybinding_row(
            &imp.fullscreen_row,
            &imp.fullscreen_label,
            "fullscreen",
            settings.read().get_keybind_article_view_fullscreen(),
        );
        self.setup_keybinding_row(
            &imp.close_row,
            &imp.close_label,
            "close_article",
            settings.read().get_keybind_article_view_close(),
        );
    }

    fn setup_keybinding_row(&self, row: &ActionRow, label: &Label, id: &str, keybinding: Option<&str>) {
        Self::keybind_label_text(keybinding, label);
        let row_name = row.widget_name().as_str().to_owned();
        let id = id.to_owned();

        let info_text = row.title();
        row.connect_activated(clone!(
            #[weak]
            label,
            #[strong]
            id,
            #[upgrade_or_panic]
            move |row| {
                if row.widget_name().as_str() == row_name {
                    let editor = KeybindingEditor::new();
                    editor.init(&info_text);
                    editor.set_transient_for(Some(&App::default().main_window()));
                    editor.present();
                    editor.connect_destroy(clone!(
                        #[weak]
                        label,
                        #[strong]
                        id,
                        #[upgrade_or_panic]
                        move |editor| {
                            let editor = editor
                                .downcast_ref::<KeybindingEditor>()
                                .expect("Failed to cast KeybindingEditor");
                            match editor.keybinding() {
                                KeybindState::Canceled | KeybindState::Illegal => {}
                                KeybindState::Disabled => {
                                    if Keybindings::write_keybinding(&id, None, &App::default().settings()).is_ok() {
                                        Self::keybind_label_text(None, &label);
                                    } else {
                                        App::default().in_app_notifiaction(&i18n("Failed to write keybinding"));
                                    }
                                }
                                KeybindState::Enabled(keybind) => {
                                    if Keybindings::write_keybinding(&id, Some(&keybind), &App::default().settings())
                                        .is_ok()
                                    {
                                        Self::keybind_label_text(Some(&keybind), &label);
                                    } else {
                                        App::default().in_app_notifiaction(&i18n("Failed to write keybinding"));
                                    }
                                }
                            }
                        }
                    ));
                }
            }
        ));
    }

    fn keybind_label_text(keybinding: Option<&str>, label: &Label) {
        let label_text = match keybinding {
            Some(keybinding) => {
                label.set_sensitive(true);
                Keybindings::parse_shortcut_string(keybinding)
                    .expect("Failed parsing saved shortcut. This should never happen!")
            }
            None => {
                label.set_sensitive(false);
                "Disabled".to_owned()
            }
        };
        label.set_label(&label_text);
    }
}
