use crate::article_view::ArticleTheme;
use glib::{clone, subclass::*};
use gtk4::{prelude::*, subclass::prelude::*, CompositeTemplate, ListBox, Popover, Widget};

mod imp {
    use super::*;
    use glib::subclass;
    use once_cell::sync::Lazy;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/settings/theme_chooser.blp")]
    pub struct ThemeChooser {
        #[template_child]
        pub theme_list: TemplateChild<ListBox>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ThemeChooser {
        const NAME: &'static str = "ThemeChooser";
        type ParentType = Popover;
        type Type = super::ThemeChooser;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ThemeChooser {
        fn signals() -> &'static [Signal] {
            static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| {
                vec![Signal::builder("article-theme-changed")
                    .param_types([ArticleTheme::static_type()])
                    .build()]
            });
            SIGNALS.as_ref()
        }

        fn constructed(&self) {
            self.theme_list.connect_row_activated(clone!(
                #[weak(rename_to = this)]
                self,
                #[upgrade_or_panic]
                move |_list, row| {
                    this.obj().popdown();

                    let theme = ArticleTheme::from_str(row.widget_name().as_str());

                    this.obj().emit_by_name::<()>("article-theme-changed", &[&theme]);
                }
            ));
        }
    }

    impl WidgetImpl for ThemeChooser {}

    impl PopoverImpl for ThemeChooser {}
}

glib::wrapper! {
    pub struct ThemeChooser(ObjectSubclass<imp::ThemeChooser>)
        @extends Widget, Popover;
}

impl Default for ThemeChooser {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl ThemeChooser {
    pub fn new() -> Self {
        Self::default()
    }
}
