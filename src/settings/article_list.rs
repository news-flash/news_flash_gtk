use glib::Enum;
use news_flash::models::ArticleOrder;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ArticleListSettings {
    pub order: ArticleOrder,
    #[serde(default)]
    pub show_thumbnails: bool,
    #[serde(default)]
    pub hide_future_articles: bool,
}

impl Default for ArticleListSettings {
    fn default() -> Self {
        ArticleListSettings {
            order: ArticleOrder::NewestFirst,
            show_thumbnails: true,
            hide_future_articles: false,
        }
    }
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, Eq, PartialEq, Enum)]
#[repr(u32)]
#[enum_type(name = "GArticleOrder")]
pub enum GArticleOrder {
    NewestFirst,
    OldestFirst,
}

impl From<u32> for GArticleOrder {
    fn from(value: u32) -> Self {
        match value {
            0 => Self::NewestFirst,
            1 => Self::OldestFirst,
            _ => Self::NewestFirst,
        }
    }
}

impl From<i32> for GArticleOrder {
    fn from(value: i32) -> Self {
        match value {
            0 => Self::NewestFirst,
            1 => Self::OldestFirst,
            _ => Self::NewestFirst,
        }
    }
}

impl From<GArticleOrder> for u32 {
    fn from(value: GArticleOrder) -> Self {
        match value {
            GArticleOrder::NewestFirst => 0,
            GArticleOrder::OldestFirst => 1,
        }
    }
}

impl From<GArticleOrder> for i32 {
    fn from(value: GArticleOrder) -> Self {
        match value {
            GArticleOrder::NewestFirst => 0,
            GArticleOrder::OldestFirst => 1,
        }
    }
}

impl From<GArticleOrder> for ArticleOrder {
    fn from(value: GArticleOrder) -> Self {
        match value {
            GArticleOrder::NewestFirst => ArticleOrder::NewestFirst,
            GArticleOrder::OldestFirst => ArticleOrder::OldestFirst,
        }
    }
}

impl From<ArticleOrder> for GArticleOrder {
    fn from(value: ArticleOrder) -> Self {
        match value {
            ArticleOrder::NewestFirst => GArticleOrder::NewestFirst,
            ArticleOrder::OldestFirst => GArticleOrder::OldestFirst,
        }
    }
}
