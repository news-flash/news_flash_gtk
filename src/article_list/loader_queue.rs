use super::{loader::LoadType, ArticleListLoader};
use crate::app::App;
use news_flash::error::NewsFlashError;
use std::{
    cell::{Cell, RefCell},
    rc::Rc,
};

#[derive(Debug, Default)]
pub struct ArticleListLoaderQueue {
    next: Rc<RefCell<Option<ArticleListLoader>>>,
    is_loading: Rc<Cell<bool>>,
}

impl ArticleListLoaderQueue {
    pub fn new() -> Self {
        Self {
            next: Rc::new(RefCell::new(None)),
            is_loading: Rc::new(Cell::new(false)),
        }
    }

    pub fn update(&self) {
        self.update_force(false);
    }

    pub fn update_forced(&self) {
        self.update_force(true);
    }

    fn update_force(&self, force_new: bool) {
        // don't replace queued 'new' with an 'update'
        let is_new_queued = self
            .next
            .borrow()
            .as_ref()
            .map(|loader| loader.load_type() == LoadType::New)
            .unwrap_or(false);
        if self.is_loading.get() && is_new_queued {
            log::debug!("skip update");
            return;
        }

        let main_window = App::default().main_window();
        let article_list = main_window.content_page().article_list_column().article_list();

        let loader = if force_new {
            ArticleListLoader::new_list()
        } else {
            ArticleListLoader::update_list()
        };

        let loader = loader
            .hide_furure_articles(App::default().settings().read().get_article_list_hide_future_articles())
            .search_term(
                App::default()
                    .content_page_state()
                    .borrow()
                    .get_search_term()
                    .map(String::from),
            )
            .order(App::default().settings().read().get_article_list_order())
            .mode(App::default().content_page_state().borrow().get_article_list_mode())
            .sidebar_selection(
                App::default()
                    .content_page_state()
                    .borrow()
                    .get_sidebar_selection()
                    .clone(),
            )
            .undo_delete_actions(main_window.content_page().undo_delete_actions())
            .last_article_date(article_list.get_last_row_model().map(|model| model.date))
            .loaded_article_ids(article_list.loaded_article_ids());

        if self.is_loading.get() {
            if let Some(old) = self.next.borrow_mut().replace(loader) {
                log::debug!("queue update - replaces old {:?}", old.load_type());
            } else {
                log::debug!("queue update");
            }
        } else {
            Self::execute_update(loader, self.next.clone(), self.is_loading.clone());
        }
    }

    fn execute_update(
        loader: ArticleListLoader,
        next: Rc<RefCell<Option<ArticleListLoader>>>,
        is_loading: Rc<Cell<bool>>,
    ) {
        is_loading.set(true);
        log::debug!("execute update");

        App::default().execute_with_callback(
            move |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    let force_new = matches!(loader.load_type(), LoadType::New);
                    let list_model = loader.build(news_flash)?;
                    Ok((list_model, force_new))
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            move |app, res| {
                if let Ok((list_model, force_new)) = res {
                    app.main_window()
                        .content_page()
                        .article_list_column()
                        .article_list()
                        .update(list_model, force_new);
                }

                let next_loader = next.borrow_mut().take();
                if let Some(loader) = next_loader {
                    log::debug!("update done: executing next item in queue");
                    match loader.load_type() {
                        LoadType::New | LoadType::Update => Self::execute_update(loader, next, is_loading.clone()),
                        LoadType::Extend => Self::execute_extend(loader, next, is_loading.clone()),
                    }
                }

                is_loading.set(false);
            },
        );
    }

    pub fn extend(&self) {
        // don't replace queued 'new' or 'update' with an 'extend'
        let is_new_or_update_queued = self
            .next
            .borrow()
            .as_ref()
            .map(|loader| loader.load_type() != LoadType::Extend)
            .unwrap_or(false);
        if self.is_loading.get() && is_new_or_update_queued {
            log::debug!("skip extend");
            return;
        }

        let main_window = App::default().main_window();
        let article_list = main_window.content_page().article_list_column().article_list();

        let loader = ArticleListLoader::extend_list()
            .hide_furure_articles(App::default().settings().read().get_article_list_hide_future_articles())
            .search_term(
                App::default()
                    .content_page_state()
                    .borrow()
                    .get_search_term()
                    .map(String::from),
            )
            .order(App::default().settings().read().get_article_list_order())
            .mode(App::default().content_page_state().borrow().get_article_list_mode())
            .sidebar_selection(
                App::default()
                    .content_page_state()
                    .borrow()
                    .get_sidebar_selection()
                    .clone(),
            )
            .undo_delete_actions(main_window.content_page().undo_delete_actions())
            .last_article_date(article_list.get_last_row_model().map(|model| model.date))
            .loaded_article_ids(article_list.loaded_article_ids());

        if self.is_loading.get() {
            if let Some(old) = self.next.borrow_mut().replace(loader) {
                log::debug!("queue extend - replaces old {:?}", old.load_type());
            } else {
                log::debug!("queue extend");
            }
        } else {
            Self::execute_extend(loader, self.next.clone(), self.is_loading.clone());
        }
    }

    fn execute_extend(
        loader: ArticleListLoader,
        next: Rc<RefCell<Option<ArticleListLoader>>>,
        is_loading: Rc<Cell<bool>>,
    ) {
        is_loading.set(true);
        log::debug!("execute extend");

        App::default().execute_with_callback(
            move |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    let list_model = loader.build(news_flash)?;
                    Ok(list_model)
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            move |app, res| {
                if let Ok(article_list_model) = res {
                    app.main_window()
                        .content_page()
                        .article_list_column()
                        .article_list()
                        .extend(article_list_model);
                }

                let next_loader = next.borrow_mut().take();
                if let Some(loader) = next_loader {
                    log::debug!("extend done: executing next item in queue");
                    match loader.load_type() {
                        LoadType::New | LoadType::Update => Self::execute_update(loader, next, is_loading.clone()),
                        LoadType::Extend => Self::execute_extend(loader, next, is_loading.clone()),
                    }
                }

                is_loading.set(false);
            },
        );
    }
}
