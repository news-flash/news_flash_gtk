mod article_header;
mod article_row;
mod article_tags;
mod loader;
mod loader_queue;
mod models;

use self::article_header::ArticleHeader;
use self::article_row::ArticleRow;
use crate::app::App;
use crate::article_list::models::GRead;
use crate::content_page::ArticleListMode;
use crate::content_page::ContentPageState;
use crate::i18n::{i18n, i18n_f};
use crate::self_stack::SelfStack;
use crate::sidebar::models::SidebarSelection;
use crate::sidebar::FeedListItemID;
use crate::util::GtkUtil;
use chrono::{DateTime, Local, Utc};
use diffus::edit::{collection, Edit};
use gio::ListStore;
use glib::{clone, SourceId};
use glib::{ControlFlow, Propagation};
use gtk4::ListHeader;
use gtk4::ListScrollFlags;
use gtk4::{
    prelude::*, subclass::prelude::*, Box, CompositeTemplate, CustomSorter, ListView, SearchBar, Shortcut,
    SignalListItemFactory, SingleSelection, SortListModel, Stack, StackTransitionType, Widget,
};
use gtk4::{CallbackAction, ConstantExpression, ListItem, PropertyExpression, TickCallbackId};
use libadwaita::StatusPage;
pub use loader::ArticleListLoader;
pub use loader_queue::ArticleListLoaderQueue;
pub use models::{ArticleGObject, ArticleListArticleModel, ArticleListModel, MarkUpdate, ReadUpdate};
use news_flash::models::{ArticleID, ArticleOrder, Marked, Read, Tag};
use std::cell::{Cell, RefCell};
use std::collections::HashMap;
use std::time::Duration;

const LIST_BOTTOM_THREASHOLD: f64 = 200.0;

#[derive(Debug)]
pub struct ScrollAnimationProperties {
    pub start_time: Option<i64>,
    pub end_time: Option<i64>,
    pub scroll_callback_id: Option<TickCallbackId>,
    pub transition_start_value: Option<f64>,
    pub transition_diff: Option<f64>,
}

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/article_list/list.blp")]
    pub struct ArticleList {
        #[template_child]
        pub stack: TemplateChild<Stack>,
        #[template_child]
        pub empty_status: TemplateChild<StatusPage>,
        #[template_child]
        pub self_stack: TemplateChild<SelfStack>,
        #[template_child]
        pub listview: TemplateChild<ListView>,
        #[template_child]
        pub factory: TemplateChild<SignalListItemFactory>,
        #[template_child]
        pub header_factory: TemplateChild<SignalListItemFactory>,
        #[template_child]
        pub selection: TemplateChild<SingleSelection>,
        #[template_child]
        pub sort_model: TemplateChild<SortListModel>,
        #[template_child]
        pub sorter: TemplateChild<CustomSorter>,
        #[template_child]
        pub section_sorter: TemplateChild<CustomSorter>,
        #[template_child]
        pub list_store: TemplateChild<ListStore>,
        #[template_child]
        pub down_shortcut: TemplateChild<Shortcut>,
        #[template_child]
        pub up_shortcut: TemplateChild<Shortcut>,
        #[template_child]
        pub activate_shortcut: TemplateChild<Shortcut>,

        pub list_model: RefCell<ArticleListModel>,
        pub model_index: RefCell<HashMap<ArticleID, ArticleGObject>>,

        pub delay_next_activation: Cell<bool>,
        pub select_after_signal: RefCell<Option<SourceId>>,
        pub selected_id: RefCell<Option<ArticleID>>,

        pub scroll_cooldown: Cell<bool>,
        pub scroll_animation_data: RefCell<ScrollAnimationProperties>,
    }

    impl Default for ArticleList {
        fn default() -> Self {
            Self {
                stack: TemplateChild::default(),
                empty_status: TemplateChild::default(),
                self_stack: TemplateChild::default(),
                listview: TemplateChild::default(),
                factory: TemplateChild::default(),
                header_factory: TemplateChild::default(),
                selection: TemplateChild::default(),
                sort_model: TemplateChild::default(),
                sorter: TemplateChild::default(),
                section_sorter: TemplateChild::default(),
                list_store: TemplateChild::default(),
                down_shortcut: TemplateChild::default(),
                up_shortcut: TemplateChild::default(),
                activate_shortcut: TemplateChild::default(),

                list_model: RefCell::new(ArticleListModel::new(
                    &App::default().settings().read().get_article_list_order(),
                )),
                model_index: RefCell::new(HashMap::new()),

                delay_next_activation: Cell::new(false),
                select_after_signal: RefCell::new(None),
                selected_id: RefCell::new(None),

                scroll_cooldown: Cell::new(false),
                scroll_animation_data: RefCell::new(ScrollAnimationProperties {
                    start_time: None,
                    end_time: None,
                    scroll_callback_id: None,
                    transition_start_value: None,
                    transition_diff: None,
                }),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ArticleList {
        const NAME: &'static str = "ArticleList";
        type ParentType = Box;
        type Type = super::ArticleList;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ArticleList {}

    impl WidgetImpl for ArticleList {}

    impl BoxImpl for ArticleList {}
}

glib::wrapper! {
    pub struct ArticleList(ObjectSubclass<imp::ArticleList>)
        @extends Widget, Box;
}

impl Default for ArticleList {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl ArticleList {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn init(&self, search_bar: &SearchBar) {
        let imp = self.imp();

        let next_action = CallbackAction::new(|_widget, _| {
            App::default()
                .main_window()
                .content_page()
                .article_list_column()
                .article_list()
                .select_next_article();
            Propagation::Stop
        });
        imp.down_shortcut.set_action(Some(next_action));

        let prev_action = CallbackAction::new(|_widget, _| {
            App::default()
                .main_window()
                .content_page()
                .article_list_column()
                .article_list()
                .select_prev_article();
            Propagation::Stop
        });
        imp.up_shortcut.set_action(Some(prev_action));

        let activate_action = CallbackAction::new(|_widget, _| {
            App::default()
                .main_window()
                .content_page()
                .responsive_layout()
                .show_article_view(true);
            Propagation::Stop
        });
        imp.activate_shortcut.set_action(Some(activate_action));

        imp.factory.connect_setup(clone!(
            #[weak(rename_to = this)]
            self,
            #[upgrade_or_panic]
            move |_factory, list_item| {
                let row = ArticleRow::new();

                let list_item = list_item.downcast_ref::<ListItem>().unwrap();
                list_item.set_child(Some(&row));

                row.connect_local(
                    "activated",
                    false,
                    clone!(
                        #[weak]
                        this,
                        #[upgrade_or_panic]
                        move |args| {
                            let row = args[1]
                                .get::<ArticleRow>()
                                .expect("The value needs to be of type `ArticleRow`.");

                            let imp = this.imp();
                            let selected_article_id = imp
                                .selection
                                .selected_item()
                                .and_then(|item| item.downcast::<ArticleGObject>().ok())
                                .map(|item| item.article_id());

                            // only activate the article if it is already selected
                            // otherwise the selection-changed signal will do the job
                            if selected_article_id == Some(row.id()) {
                                Self::activate_article(row.id(), row.read());
                            }

                            None
                        }
                    ),
                );

                row.connect_destroy(|row| row.teardown_row());

                // Create expression describing `list_item->item`
                let list_item_expression = ConstantExpression::new(list_item);
                let article_gobject_expression =
                    PropertyExpression::new(ListItem::static_type(), Some(&list_item_expression), "item");

                // Update Read
                let read_expression =
                    PropertyExpression::new(ArticleGObject::static_type(), Some(&article_gobject_expression), "read");
                read_expression.bind(&row, "read", Some(&row));

                // Update Marked
                let marked_expression = PropertyExpression::new(
                    ArticleGObject::static_type(),
                    Some(&article_gobject_expression),
                    "marked",
                );
                marked_expression.bind(&row, "marked", Some(&row));

                // Update Date
                let date_expression = PropertyExpression::new(
                    ArticleGObject::static_type(),
                    Some(&article_gobject_expression),
                    "date-string",
                );
                date_expression.bind(&row, "date", Some(&row));

                // Update Tags
                let tags_expression =
                    PropertyExpression::new(ArticleGObject::static_type(), Some(&article_gobject_expression), "tags");
                tags_expression.bind(&row, "tags", Some(&row));

                // Update Thumbnail
                let thumbnail_expression = PropertyExpression::new(
                    ArticleGObject::static_type(),
                    Some(&article_gobject_expression),
                    "thumbnail",
                );
                thumbnail_expression.bind(&row, "thumbnail", Some(&row));

                // Update feed name
                let feed_title_expression = PropertyExpression::new(
                    ArticleGObject::static_type(),
                    Some(&article_gobject_expression),
                    "feed-title",
                );
                feed_title_expression.bind(&row, "feed-title", Some(&row));

                // Update selected
                let selected_expression = PropertyExpression::new(
                    ArticleGObject::static_type(),
                    Some(&article_gobject_expression),
                    "selected",
                );
                selected_expression.bind(&row, "selected", Some(&row));
            }
        ));
        imp.factory.connect_bind(move |_factory, list_item| {
            let list_item = list_item.downcast_ref::<ListItem>().unwrap();
            let article = list_item.item().and_downcast::<ArticleGObject>().unwrap();
            let child = list_item.child().and_downcast::<ArticleRow>().unwrap();
            child.bind_model(&article);
        });
        imp.factory.connect_unbind(|_factory, list_item| {
            let list_item = list_item.downcast_ref::<ListItem>().unwrap();
            let child = list_item.child().and_downcast::<ArticleRow>().unwrap();
            child.unbind_model();
        });

        imp.header_factory.connect_setup(|_factory, list_header| {
            let header = ArticleHeader::new();
            let list_header = list_header.downcast_ref::<ListHeader>().unwrap();
            list_header.set_child(Some(&header));
        });
        imp.header_factory.connect_bind(move |_factory, list_header| {
            let list_header = list_header.downcast_ref::<ListHeader>().unwrap();
            let article = list_header.item().and_downcast::<ArticleGObject>().unwrap();
            let child = list_header.child().and_downcast::<ArticleHeader>().unwrap();
            child.bind_model(&article);
        });

        let settings = App::default().settings();
        imp.sorter.set_sort_func(move |obj1, obj2| {
            let date_1: DateTime<Utc> = obj1.downcast_ref::<ArticleGObject>().unwrap().date().into();
            let date_2: DateTime<Utc> = obj2.downcast_ref::<ArticleGObject>().unwrap().date().into();

            match settings.read().get_article_list_order() {
                ArticleOrder::NewestFirst => date_2.cmp(&date_1).into(),
                ArticleOrder::OldestFirst => date_1.cmp(&date_2).into(),
            }
        });
        let settings = App::default().settings();
        imp.section_sorter.set_sort_func(move |obj1, obj2| {
            let obj1 = obj1.downcast_ref::<ArticleGObject>().unwrap();
            let obj2 = obj2.downcast_ref::<ArticleGObject>().unwrap();

            let date_1: DateTime<Utc> = obj1.date().into();
            let date_2: DateTime<Utc> = obj2.date().into();

            let date_1 = date_1.with_timezone(&Local);
            let date_2 = date_2.with_timezone(&Local);

            let date_1 = date_1.date_naive();
            let date_2 = date_2.date_naive();

            match settings.read().get_article_list_order() {
                ArticleOrder::NewestFirst => date_2.cmp(&date_1).into(),
                ArticleOrder::OldestFirst => date_1.cmp(&date_2).into(),
            }
        });
        imp.selection.connect_selection_changed(clone!(
            #[weak(rename_to = this)]
            self,
            #[weak]
            search_bar,
            #[upgrade_or_panic]
            move |selection_model, pos, n_items| {
                let imp = this.imp();
                let selected = selection_model.selected();

                for i in pos..pos + n_items {
                    if let Some(item) = selection_model.item(i).and_downcast::<ArticleGObject>() {
                        item.set_selected(false);
                    }
                }

                if selected == gtk4::INVALID_LIST_POSITION {
                    return;
                }

                let article_gobject = selection_model.item(selected).and_downcast::<ArticleGObject>().unwrap();
                article_gobject.set_selected(true);

                let id = article_gobject.article_id();
                let read = article_gobject.read();

                if !imp.delay_next_activation.get() {
                    Self::activate_article(id, read);
                } else if let Some(article_gobject) = this.get_selected_article_model() {
                    if read == GRead::Unread {
                        let update = ReadUpdate {
                            article_id: article_gobject.article_id(),
                            read: Read::Read,
                        };
                        App::default().mark_article_read(update);
                    }

                    GtkUtil::remove_source(imp.select_after_signal.take());
                    imp.delay_next_activation.set(false);

                    imp.select_after_signal.replace(Some(glib::timeout_add_local(
                        Duration::from_millis(300),
                        clone!(
                            #[weak]
                            this,
                            #[weak]
                            search_bar,
                            #[strong]
                            id,
                            #[strong]
                            read,
                            #[upgrade_or_panic]
                            move || {
                                let imp = this.imp();

                                if search_bar.has_focus() {
                                    return ControlFlow::Break;
                                }

                                Self::activate_article(id.clone(), read);
                                imp.select_after_signal.take();
                                ControlFlow::Break
                            }
                        ),
                    )));
                }
            }
        ));

        if let Some(vadj) = imp.listview.vadjustment() {
            vadj.connect_value_changed(clone!(
                #[weak(rename_to = this)]
                self,
                #[upgrade_or_panic]
                move |vadj| {
                    let imp = this.imp();

                    let is_on_cooldown = imp.scroll_cooldown.get();
                    if !is_on_cooldown {
                        let max = vadj.upper() - vadj.page_size();
                        if max > 0.0 && vadj.value() >= (max - LIST_BOTTOM_THREASHOLD) {
                            imp.scroll_cooldown.set(true);
                            glib::source::timeout_add_local(
                                Duration::from_millis(800),
                                clone!(
                                    #[weak]
                                    vadj,
                                    #[weak]
                                    this,
                                    #[upgrade_or_panic]
                                    move || {
                                        let imp = this.imp();

                                        imp.scroll_cooldown.set(false);
                                        let max = vadj.upper() - vadj.page_size();
                                        if max > 0.0 && vadj.value() >= (max - (LIST_BOTTOM_THREASHOLD / 4.0)) {
                                            App::default().article_list_extend();
                                        }
                                        ControlFlow::Break
                                    }
                                ),
                            );
                            App::default().article_list_extend();
                        }
                    }
                }
            ));
        }
    }

    fn activate_article(article_id: ArticleID, read: GRead) {
        App::default()
            .main_window()
            .content_page()
            .visible_article_manager()
            .load_article_from_db(article_id, read == GRead::Unread);
    }

    fn is_empty(&self) -> bool {
        let imp = self.imp();
        imp.list_store.n_items() == 0
    }

    pub fn set_transition(&self, transition: StackTransitionType) {
        self.imp().stack.set_transition_type(transition);
    }

    pub fn update(&self, new_list: ArticleListModel, is_new_list: bool) {
        let imp = self.imp();

        let require_new_list = is_new_list || self.is_empty();

        // check if list model is empty and display a message
        if new_list.len() == 0 {
            let state = App::default().content_page_state();
            let message = Self::compose_empty_message(&state.borrow());
            imp.empty_status
                .set_description(Some(glib::markup_escape_text(&message).as_str()));

            if !self.is_empty() {
                imp.stack.set_visible_child_name("empty");
                imp.list_store.remove_all();
                imp.model_index.borrow_mut().clear();
            }
        } else {
            // check if a new list is reqired or current list should be updated
            if require_new_list {
                // compare with empty model
                let empty_list = ArticleListModel::new(&new_list.order());
                let diff = empty_list.generate_diff(&new_list);

                if self.is_empty() {
                    self.execute_diff(diff);
                    imp.stack.set_visible_child_name("list");
                } else {
                    imp.self_stack.freeze();
                    imp.list_store.remove_all();
                    imp.model_index.borrow_mut().clear();
                    self.execute_diff(diff);
                    imp.self_stack.update(imp.stack.transition_type());
                }
            } else {
                let model_guard = imp.list_model.borrow();
                let diff = model_guard.generate_diff(&new_list);
                imp.self_stack.freeze();
                self.execute_diff(diff);
                imp.self_stack.update(imp.stack.transition_type());
            }
        }

        imp.list_model.replace(new_list);

        // reset transition to default for next update
        self.set_transition(StackTransitionType::Crossfade);

        // #[cfg(debug_assertions)]
        // self.check_list_integrity();
    }

    // #[cfg(debug_assertions)]
    // fn check_list_integrity(&self) {
    //     if self.is_empty() {
    //         return;
    //     }

    //     let mut table : String = "| nr | list store | model | unread |\n| --- | --- | --- | --- |".into();

    //     let imp = self.imp();
    //     let mut item = imp.listview.first_child().unwrap();
    //     let mut pos = 0;

    //     loop {
    //         let list_item = match imp.list_store.item(pos).and_downcast::<ArticleGObject>() {
    //             Some(item) => item,
    //             None => break,
    //         };
    //         let model_id = imp.list_model.borrow().get(pos as usize).map(|model| model.id.clone());

    //         if Some(list_item.article_id()) != model_id {
    //             log::warn!("stranger danger! list_store: {} - model: {:?}", list_item.article_id(), model_id);
    //         }

    //         table.push_str(&format!("\n| {} | {} | {:?} | {:?} |", pos, list_item.article_id(), model_id, list_item.read()));

    //         pos += 1;

    //         item = match item.next_sibling() {
    //             Some(next) => next,
    //             None => break,
    //         };
    //     }

    //     #[allow(deprecated)]
    //     std::fs::write(&format!("{}/dbg.md", std::env::home_dir().unwrap().display()), table).unwrap();
    // }

    pub fn extend(&self, list_to_append: ArticleListModel) {
        let imp = self.imp();

        let g_articles = list_to_append
            .models()
            .iter()
            .map(ArticleGObject::from_model)
            .collect::<Vec<ArticleGObject>>();

        imp.list_store.splice(imp.list_store.n_items(), 0, &g_articles);

        for g_article in g_articles {
            if !imp.list_model.borrow().contains(&g_article.article_id()) {
                imp.model_index.borrow_mut().insert(g_article.article_id(), g_article);
            } else {
                log::warn!("trying to add article which already exists: {}", g_article.article_id());
            }
        }

        imp.list_model.borrow_mut().add_models(list_to_append.models().clone());

        // #[cfg(debug_assertions)]
        // self.check_list_integrity();
    }

    fn execute_diff(&self, diff: Edit<'_, Vec<ArticleListArticleModel>>) {
        let imp = self.imp();
        let mut pos = 0;
        let mut model_index_guard = imp.model_index.borrow_mut();
        let mut article_insert_batch: Vec<ArticleID> = Vec::new();

        match diff {
            Edit::Copy(_list) => { /* no difference */ }
            Edit::Change(diff) => {
                let mut iter = diff.into_iter().peekable();

                loop {
                    let edit = iter.next();

                    if let Some(edit) = edit {
                        match edit {
                            collection::Edit::Copy(_article) => {
                                // nothing changed
                                pos += 1;
                            }
                            collection::Edit::Insert(article) => {
                                let article_id = article.id.clone();
                                let article_gobject = ArticleGObject::from_model(article);

                                // if the next edit will be an insert as well add it to g_article_vec
                                // and insert all g_articles at once as soon as the next edit
                                // is something other than insert
                                if let Some(collection::Edit::Insert(_next_article)) = iter.peek() {
                                    article_insert_batch.push(article_gobject.article_id().clone());
                                    model_index_guard.insert(article_id, article_gobject);
                                } else if !article_insert_batch.is_empty() {
                                    article_insert_batch.push(article_gobject.article_id().clone());
                                    model_index_guard.insert(article_id, article_gobject);

                                    let g_article_to_insert = article_insert_batch
                                        .iter()
                                        .filter_map(|id| model_index_guard.get(id))
                                        .cloned()
                                        .collect::<Vec<ArticleGObject>>();

                                    imp.list_store.splice(pos as u32, 0, &g_article_to_insert);
                                    pos += article_insert_batch.len();

                                    article_insert_batch.clear();
                                } else {
                                    imp.list_store.insert(pos as u32, &article_gobject);
                                    model_index_guard.insert(article_id, article_gobject);
                                    pos += 1;
                                }
                            }
                            collection::Edit::Remove(article) => {
                                let remove_pos = model_index_guard
                                    .get(&article.id)
                                    .and_then(|article_gobject| imp.list_store.find(article_gobject));
                                if let Some(remove_pos) = remove_pos {
                                    imp.list_store.remove(remove_pos);
                                    model_index_guard.remove(&article.id);
                                }
                            }
                            collection::Edit::Change(diff) => {
                                if let Some(article_gobject) = model_index_guard.get(&diff.id) {
                                    if let Some(read) = diff.read {
                                        article_gobject.set_read(read);
                                    }
                                    if let Some(marked) = diff.marked {
                                        article_gobject.set_marked(marked);
                                    }
                                    if let Some(timestamp) = diff.date {
                                        article_gobject.set_date(timestamp);
                                    }
                                    if let Some(tags) = diff.tags {
                                        article_gobject.set_tags(tags);
                                    }
                                    if let Some(thumbnail) = diff.thumbnail {
                                        article_gobject.set_thumbnail(thumbnail);
                                    }
                                    if let Some(feed_title) = diff.feed_title {
                                        article_gobject.set_feed_title(feed_title);
                                    }
                                }
                                pos += 1;
                            }
                        }
                    } else {
                        break;
                    }
                }
            }
        }
    }

    fn compose_empty_message(new_state: &ContentPageState) -> String {
        match new_state.get_sidebar_selection() {
            SidebarSelection::All => match new_state.get_article_list_mode() {
                ArticleListMode::All => match new_state.get_search_term() {
                    Some(search) => i18n_f("No articles that fit \"{}\"", &[search]),
                    None => i18n("No Articles"),
                },
                ArticleListMode::Unread => match new_state.get_search_term() {
                    Some(search) => i18n_f("No unread articles that fit \"{}\"", &[search]),
                    None => i18n("No Unread Articles"),
                },
                ArticleListMode::Marked => match new_state.get_search_term() {
                    Some(search) => i18n_f("No starred articles that fit \"{}\"", &[search]),
                    None => i18n("No Starred Articles"),
                },
            },
            SidebarSelection::FeedList(id, title) => {
                let item = match id {
                    FeedListItemID::Category(_) => "category",
                    FeedListItemID::Feed(..) => "feed",
                };
                match new_state.get_article_list_mode() {
                    ArticleListMode::All => match new_state.get_search_term() {
                        Some(search) => i18n_f(
                            "No articles that fit \"{}\" in {} \"{}\"",
                            &[search, item, title.as_str()],
                        ),
                        None => i18n_f("No articles in {} \"{}\"", &[item, title.as_str()]),
                    },
                    ArticleListMode::Unread => match new_state.get_search_term() {
                        Some(search) => i18n_f(
                            "No unread articles that fit \"{}\" in {} \"{}\"",
                            &[search, item, title],
                        ),
                        None => i18n_f("No unread articles in {} \"{}\"", &[item, title.as_str()]),
                    },
                    ArticleListMode::Marked => match new_state.get_search_term() {
                        Some(search) => i18n_f(
                            "No starred articles that fit \"{}\" in {} \"{}\"",
                            &[search, item, title.as_str()],
                        ),
                        None => i18n_f("No starred articles in {} \"{}\"", &[item, title.as_str()]),
                    },
                }
            }
            SidebarSelection::Tag(_id, title) => match new_state.get_article_list_mode() {
                ArticleListMode::All => match new_state.get_search_term() {
                    Some(search) => i18n_f("No articles that fit \"{}\" in tag \"{}\"", &[search, title.as_str()]),
                    None => i18n_f("No articles in tag \"{}\"", &[title.as_str()]),
                },
                ArticleListMode::Unread => match new_state.get_search_term() {
                    Some(search) => i18n_f(
                        "No unread articles that fit \"{}\" in tag \"{}\"",
                        &[search, title.as_str()],
                    ),
                    None => i18n_f("No unread articles in tag \"{}\"", &[title.as_str()]),
                },
                ArticleListMode::Marked => match new_state.get_search_term() {
                    Some(search) => i18n_f(
                        "No starred articles that fit \"{}\" in tag \"{}\"",
                        &[search, title.as_str()],
                    ),
                    None => i18n_f("No starred articles in tag \"{}\"", &[title.as_str()]),
                },
            },
        }
    }

    pub fn select_next_article(&self) {
        self.select_article_dir(1)
    }

    pub fn select_prev_article(&self) {
        self.select_article_dir(-1)
    }

    fn select_article_dir(&self, direction: i32) {
        let imp = self.imp();

        if !self.is_empty() {
            imp.listview.grab_focus();
            let inner_splitview = App::default().main_window().content_page().inner().clone();

            if inner_splitview.is_collapsed() && !inner_splitview.shows_content() {
                App::default()
                    .content_page_state()
                    .borrow_mut()
                    .set_article_view_visible(false);
            }

            let selected_item_pos = imp.selection.selected();
            if selected_item_pos == gtk4::INVALID_LIST_POSITION {
                return self.select_first();
            }

            let new_item_pos = selected_item_pos as i32 + direction;

            if new_item_pos < 0 || new_item_pos as u32 == gtk4::INVALID_LIST_POSITION {
                return self.select_first();
            }

            // select now, but activate 300ms later
            imp.delay_next_activation.set(
                !inner_splitview.is_collapsed() || (inner_splitview.is_collapsed() && !inner_splitview.shows_content()),
            );
            imp.selection.select_item(new_item_pos as u32, true);
            imp.listview.scroll_to(new_item_pos as u32, ListScrollFlags::NONE, None);
        }
    }

    fn select_first(&self) {
        let imp = self.imp();

        imp.delay_next_activation.set(true);
        imp.selection.select_item(0, true);
        imp.listview.scroll_to(0, ListScrollFlags::NONE, None);
    }

    pub fn get_selected_article_model(&self) -> Option<ArticleGObject> {
        if !self.is_empty() {
            let imp = self.imp();
            imp.selection
                .selected_item()
                .and_then(|item| item.downcast::<ArticleGObject>().ok())
        } else {
            None
        }
    }

    pub fn set_article_row_state(&self, article_id: &ArticleID, read: Option<Read>, marked: Option<Marked>) {
        if !self.is_empty() {
            let imp = self.imp();
            if let Some(article_gobject) = imp.model_index.borrow().get(article_id) {
                if let Some(read) = read {
                    article_gobject.set_read(read);
                    imp.list_model.borrow_mut().set_read(article_id, read);
                }
                if let Some(marked) = marked {
                    article_gobject.set_marked(marked);
                    imp.list_model.borrow_mut().set_marked(article_id, marked);
                }
            }
        }
    }

    pub fn article_row_update_tags(&self, article_id: &ArticleID, add: Option<&Tag>, remove: Option<&Tag>) {
        if !self.is_empty() {
            let imp = self.imp();
            if let Some(article_gobject) = imp.model_index.borrow().get(article_id) {
                if let Some(tag) = remove {
                    if let Some(tags) = imp.list_model.borrow_mut().remove_tag(article_id, tag) {
                        article_gobject.set_tags(tags.clone());
                    }
                }
                if let Some(tag) = add {
                    if let Some(tags) = imp.list_model.borrow_mut().add_tag(article_id, tag) {
                        article_gobject.set_tags(tags.clone());
                    }
                }
            }
        }
    }

    pub fn get_last_row_model(&self) -> Option<ArticleListArticleModel> {
        self.imp().list_model.borrow().last().cloned()
    }

    pub fn get_model(&self, article_id: &ArticleID) -> Option<ArticleGObject> {
        self.imp().model_index.borrow().get(article_id).cloned()
    }

    pub fn loaded_article_ids(&self) -> Vec<ArticleID> {
        self.imp().model_index.borrow().keys().cloned().collect()
    }
}
