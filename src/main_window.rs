use crate::about_dialog::APP_NAME;
use crate::app::App;
use crate::config::{APP_ID, PROFILE};
use crate::content_page::{ContentPage, ContentPageState};
use crate::login_screen::{CustomApiSecret, PasswordLogin, WebLogin};
use crate::main_window_actions::MainWindowActions;
use crate::reset_page::ResetPage;
use crate::sidebar::models::SidebarSelection;
use crate::sidebar::FeedListItemID;
use crate::undo_delete_action::UndoDelete;
use crate::video_dialog::VideoDialog;
use crate::welcome_screen::WelcomePage;
use gdk4::RGBA;
use glib::{self, clone, subclass, ControlFlow, Propagation};
use gtk4::{self, prelude::*, subclass::prelude::*, CompositeTemplate};
use libadwaita::prelude::*;
use libadwaita::{subclass::prelude::*, NavigationPage, NavigationView};
use news_flash::models::{ApiSecret, Enclosure, LoginData, LoginGUI, PluginID, PluginInfo};
use news_flash::{error::NewsFlashError, NewsFlash};
use std::cell::RefCell;
use std::rc::Rc;
use std::time::Duration;

const CONTENT_PAGE: &str = "content_page";
const WELCOME_PAGE: &str = "welcome_page";
const RESET_PAGE: &str = "reset_page";
const WEB_PAGE: &str = "web_login";
const PASSWORD_PAGE: &str = "password_login";
const API_SECRET_PAGE: &str = "api_secret_page";
const VIDEO_PAGE: &str = "video_page";

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/main_window.blp")]
    pub struct MainWindow {
        #[template_child]
        pub navigation_view: TemplateChild<NavigationView>,
        #[template_child]
        pub welcome_page: TemplateChild<WelcomePage>,
        #[template_child]
        pub reset_page: TemplateChild<ResetPage>,
        #[template_child]
        pub content_page: TemplateChild<ContentPage>,
        #[template_child]
        pub password_login: TemplateChild<PasswordLogin>,
        #[template_child]
        pub web_login: TemplateChild<WebLogin>,
        #[template_child]
        pub api_secret_page: TemplateChild<CustomApiSecret>,
        #[template_child]
        pub video: TemplateChild<VideoDialog>,
        #[template_child]
        pub video_page: TemplateChild<NavigationPage>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for MainWindow {
        const NAME: &'static str = "MainWindow";
        type ParentType = libadwaita::ApplicationWindow;
        type Type = super::MainWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for MainWindow {}

    impl WidgetImpl for MainWindow {}

    impl WindowImpl for MainWindow {}

    impl ApplicationWindowImpl for MainWindow {}

    impl AdwApplicationWindowImpl for MainWindow {}
}

glib::wrapper! {
    pub struct MainWindow(ObjectSubclass<imp::MainWindow>)
        @extends gtk4::Widget, gtk4::Window, gtk4::ApplicationWindow, libadwaita::ApplicationWindow,
        @implements gio::ActionMap, gio::ActionGroup;
}

impl Default for MainWindow {
    fn default() -> Self {
        Self::new()
    }
}

impl MainWindow {
    pub fn new() -> Self {
        let window = glib::Object::new::<Self>();
        window.set_icon_name(Some(APP_ID));
        window.set_title(Some(APP_NAME));

        if let Ok(state) = ContentPageState::new_from_file() {
            let (width, height) = state.get_window_size();
            window.set_default_size(width, height);

            if state.get_maximized() {
                window.maximize();
            }
        }

        if PROFILE == "Devel" {
            window.add_css_class("devel");
        }
        window
    }

    pub fn init(&self, shutdown_in_progress: Rc<RefCell<bool>>) {
        let imp = self.imp();

        libadwaita::StyleManager::default().connect_dark_notify(clone!(
            #[weak(rename_to = window)]
            self,
            #[upgrade_or_panic]
            move |sm| {
                let is_dark = sm.is_dark();
                glib::timeout_add_local(Duration::from_millis(20), move || {
                    window.content_page().load_branding();
                    let rgba = if is_dark { RGBA::BLACK } else { RGBA::WHITE };
                    window
                        .content_page()
                        .articleview_column()
                        .article_view()
                        .update_background_color(&rgba);
                    ControlFlow::Break
                });
            }
        ));

        // setup shutdown
        self.connect_close_request(clone!(
            #[strong]
            shutdown_in_progress,
            #[weak(rename_to = window)]
            self,
            #[upgrade_or_panic]
            move |win| {
                if window.is_page_visible(VIDEO_PAGE) {
                    window.pop_video();
                }

                if *shutdown_in_progress.borrow() {
                    win.set_visible(false);
                    return Propagation::Stop;
                }
                if App::default().settings().read().get_keep_running_in_background()
                    && window.is_page_visible(CONTENT_PAGE)
                {
                    win.set_visible(false);
                    return Propagation::Stop;
                }

                App::default().queue_quit();
                Propagation::Stop
            }
        ));

        self.connect_fullscreened_notify(|window| {
            let window_fullscreen = window.is_fullscreen();
            window.content_page().outer().set_collapsed(window_fullscreen);
            window.content_page().inner().set_collapsed(window_fullscreen);
            window.content_page().inner().set_show_content(window_fullscreen);
            window.video().show_top_bar(!window_fullscreen);
        });

        self.connect_visible_dialog_notify(|window| {
            window
                .content_page()
                .state()
                .borrow_mut()
                .set_dialog_open(window.visible_dialog().is_some());
            MainWindowActions::update_state();
        });

        imp.video_page.connect_hiding(clone!(
            #[weak(rename_to = window)]
            self,
            #[upgrade_or_panic]
            move |_page| {
                window.imp().video.stop();
            }
        ));

        MainWindowActions::setup();

        // setup background permissions
        if App::default().settings().read().get_keep_running_in_background() {
            App::request_background_permission(App::default().settings().read().get_autostart());
        }

        // set visible page
        imp.navigation_view.connect_visible_page_notify(clone!(
            #[weak(rename_to = window)]
            self,
            #[upgrade_or_panic]
            move |_navigation_view| {
                let is_content_page = window.is_page_visible(CONTENT_PAGE);
                App::default()
                    .content_page_state()
                    .borrow_mut()
                    .set_content_page_visible(is_content_page);
                MainWindowActions::update_state();
            }
        ));
        imp.navigation_view.replace_with_tags(&[CONTENT_PAGE]);
        self.content_page().load_branding();
    }

    pub fn content_page(&self) -> &ContentPage {
        let imp = self.imp();
        &imp.content_page
    }

    pub fn show_undo_toast(&self, action: UndoDelete) {
        let select_all_button = match self.content_page().sidebar_column().sidebar().get_selection() {
            SidebarSelection::All => false,
            SidebarSelection::FeedList(selected_id, _label) => match &action {
                UndoDelete::Category(delete_id, _label) => selected_id == FeedListItemID::Category(delete_id.clone()),
                UndoDelete::Feed(delete_id, _label) => {
                    if let FeedListItemID::Feed(mapping) = &selected_id {
                        &mapping.feed_id == delete_id
                    } else {
                        false
                    }
                }
                _ => false,
            },

            SidebarSelection::Tag(selected_id, _label) => match &action {
                UndoDelete::Tag(delete_id, _label) => &selected_id == delete_id,
                _ => false,
            },
        };

        if select_all_button {
            self.content_page()
                .state()
                .borrow_mut()
                .set_sidebar_selection(SidebarSelection::All);
            self.content_page()
                .sidebar_column()
                .sidebar()
                .select_all_button_no_update();
        }

        self.content_page().add_undo_delete_notification(action);
    }

    pub fn show_welcome_page(&self) {
        let imp = self.imp();

        imp.navigation_view.replace_with_tags(&[WELCOME_PAGE]);
    }

    pub fn show_login_page(&self, plugin_id: &PluginID, data: Option<LoginData>) {
        let imp = self.imp();

        imp.web_login.reset();
        imp.password_login.reset();
        imp.api_secret_page.reset();

        if let Some(info) = NewsFlash::list_backends().get(plugin_id) {
            match &info.login_gui {
                LoginGUI::OAuth(oauth_info) => {
                    if oauth_info.custom_api_secret {
                        imp.api_secret_page.set_service(info);

                        if let Some(LoginData::OAuth(oauth_data)) = data {
                            if let Some(api_secret) = &oauth_data.custom_api_secret {
                                imp.api_secret_page.fill(api_secret);
                            } else {
                                imp.api_secret_page.reset();
                            }
                        }

                        imp.navigation_view.push_by_tag(API_SECRET_PAGE);
                    } else if let Ok(()) = imp.web_login.set_service(info) {
                        imp.navigation_view.push_by_tag(WEB_PAGE);
                    }
                }
                LoginGUI::Direct(_) => {
                    if let Ok(()) = imp.password_login.set_service(info) {
                        if let Some(LoginData::Direct(data)) = data {
                            imp.password_login.fill(data);
                        } else {
                            imp.password_login.reset();
                        }
                        imp.navigation_view.push_by_tag(PASSWORD_PAGE);
                    }
                }
                LoginGUI::None => {}
            }
        }
    }

    pub fn show_web_login_page(&self, info: &PluginInfo, secret: Option<&ApiSecret>) {
        let imp = self.imp();

        _ = imp.web_login.set_service(info);
        imp.web_login.set_custom_api_secret(secret);
        imp.navigation_view.push_by_tag(WEB_PAGE);
    }

    pub fn show_login_error(&self, error: NewsFlashError, data: &LoginData) {
        let imp = self.imp();

        match &data {
            LoginData::OAuth(_) => imp.web_login.show_error(error),
            LoginData::Direct(_) => imp.password_login.show_error(error),
            LoginData::None(_) => {}
        }
    }

    pub fn show_content_page(&self) {
        let imp = self.imp();

        imp.navigation_view.replace_with_tags(&[CONTENT_PAGE]);

        App::default().update_sidebar();

        // show discover dialog after login with local rss
        App::default().execute_with_callback(
            |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash.id().await
                } else {
                    None
                }
            },
            |app, res| {
                if res.map(|id| id.as_str() == "local_rss").unwrap_or(false) {
                    app.spawn_discover_dialog();
                }
            },
        );

        self.content_page().load_branding();
    }

    pub fn show_reset_page(&self) {
        let imp = self.imp();
        imp.reset_page.reset();
        imp.navigation_view.push_by_tag(RESET_PAGE);
    }

    pub fn reset_account_failed(&self, error: NewsFlashError) {
        let imp = self.imp();
        imp.reset_page.error(error);
    }

    pub fn set_sidebar_selection(&self, selection: SidebarSelection) {
        self.content_page().responsive_layout().show_sidebar();

        if &selection != self.content_page().state().borrow_mut().get_sidebar_selection() {
            self.content_page()
                .state()
                .borrow_mut()
                .set_sidebar_selection(selection);
            App::default().article_list_new();
        }
    }

    pub fn update_features(&self) {
        self.content_page().sidebar_column().update_features();
    }

    pub fn video(&self) -> &VideoDialog {
        &self.imp().video
    }

    pub fn play_enclosure(&self, enclosure: &Enclosure) {
        let imp = self.imp();
        imp.navigation_view.push_by_tag(VIDEO_PAGE);
        imp.video.play(enclosure);
    }

    pub fn pop_video(&self) {
        if self.is_page_visible(VIDEO_PAGE) {
            self.imp().navigation_view.pop();
        }
    }

    fn is_page_visible(&self, page_tag: &str) -> bool {
        self.imp()
            .navigation_view
            .visible_page()
            .and_then(|page| page.tag().map(|tag| tag.as_str() == page_tag))
            .unwrap_or(false)
    }
}
