use news_flash::models::ArticleID;

#[derive(Debug, Clone)]
pub struct UndoMarkReadAction {
    pub article_ids: Vec<ArticleID>,
}
