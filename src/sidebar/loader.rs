use super::{FeedListTree, TagListModel};
use crate::{content_page::ArticleListMode, undo_delete_action::UndoDelete};
use news_flash::{
    error::NewsFlashError,
    models::{CategoryID, CategoryMapping, Feed, FeedID, FeedMapping, NEWSFLASH_TOPLEVEL},
    NewsFlash,
};
use std::collections::{HashMap, HashSet};

pub struct SidebarLoader {
    hide_future_articles: bool,
    article_list_mode: ArticleListMode,
    undo_delete_actions: Vec<UndoDelete>,
}

impl SidebarLoader {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn hide_future_articles(mut self, hide_future_articles: bool) -> Self {
        self.hide_future_articles = hide_future_articles;
        self
    }

    pub fn article_list_mode(mut self, article_list_mode: ArticleListMode) -> Self {
        self.article_list_mode = article_list_mode;
        self
    }

    pub fn undo_delete_actions(mut self, undo_delete_actions: Vec<UndoDelete>) -> Self {
        self.undo_delete_actions = undo_delete_actions;
        self
    }

    pub fn load(self, news_flash: &NewsFlash) -> Result<(i64, FeedListTree, TagListModel), NewsFlashError> {
        let mut tree = FeedListTree::new();
        let mut tag_list_model = TagListModel::new();

        let (categories, category_mappings) = news_flash.get_categories()?;
        let (feeds, mut feed_mappings) = news_flash.get_feeds()?;

        // collect unread and marked counts
        let feed_count_map = match self.article_list_mode {
            ArticleListMode::All | ArticleListMode::Unread => {
                news_flash.unread_count_feed_map(self.hide_future_articles)?
            }
            ArticleListMode::Marked => news_flash.marked_count_feed_map()?,
        };

        let mut pending_delte_feeds = HashSet::new();
        let mut pending_delete_categories = HashSet::new();
        let mut pending_delete_tags = HashSet::new();

        for undo_delete_action in self.undo_delete_actions {
            match undo_delete_action {
                UndoDelete::Feed(id, _label) => pending_delte_feeds.insert(id),
                UndoDelete::Category(id, _label) => pending_delete_categories.insert(id),
                UndoDelete::Tag(id, _label) => pending_delete_tags.insert(id),
            };
        }

        // If there are feeds without a category:
        // - create mappings for all feeds without category to be children of the toplevel
        let mut uncategorized_mappings = Self::create_mappings_for_uncategorized_feeds(&feeds, &feed_mappings);
        feed_mappings.append(&mut uncategorized_mappings);

        // feedlist: Categories
        let mut pending_categories = Vec::new();
        for mapping in &category_mappings {
            if pending_delete_categories.contains(&mapping.category_id) {
                continue;
            }

            let category = match categories.iter().find(|c| c.category_id == mapping.category_id) {
                Some(res) => res,
                None => {
                    log::warn!(
                        "Mapping for category '{}' exists, but can't find the category itself",
                        mapping.category_id
                    );
                    continue;
                }
            };

            let category_item_count = Self::calculate_item_count_for_category(
                &category.category_id,
                &feed_mappings,
                &category_mappings,
                &feed_count_map,
                &pending_delte_feeds,
                &pending_delete_categories,
            );

            pending_categories.push((category, mapping, category_item_count));
        }
        tree.add_categories(&pending_categories);

        // feedlist: Feeds
        for mapping in &feed_mappings {
            if pending_delte_feeds.contains(&mapping.feed_id)
                || pending_delete_categories.contains(&mapping.category_id)
            {
                continue;
            }

            let feed = match feeds.iter().find(|feed| feed.feed_id == mapping.feed_id) {
                Some(res) => res,
                None => {
                    log::warn!(
                        "Mapping for feed '{}' exists, but can't find the feed itself",
                        mapping.feed_id
                    );
                    continue;
                }
            };

            let item_count = match feed_count_map.get(&mapping.feed_id) {
                Some(count) => *count,
                None => 0,
            };
            if tree.add_feed(feed, mapping, item_count).is_err() {}
        }

        // tag list
        let (tags, _taggings) = news_flash.get_tags()?;
        for tag in tags {
            if pending_delete_tags.contains(&tag.tag_id) {
                continue;
            }
            _ = tag_list_model.add(&tag);
        }

        let total_item_count = Self::calculate_item_count_for_category(
            &NEWSFLASH_TOPLEVEL,
            &feed_mappings,
            &category_mappings,
            &feed_count_map,
            &pending_delte_feeds,
            &pending_delete_categories,
        );

        Ok((total_item_count, tree, tag_list_model))
    }

    fn calculate_item_count_for_category(
        category_id: &CategoryID,
        feed_mappings: &[FeedMapping],
        category_mappings: &[CategoryMapping],
        item_count_map: &HashMap<FeedID, i64>,
        pending_deleted_feeds: &HashSet<FeedID>,
        pending_deleted_categories: &HashSet<CategoryID>,
    ) -> i64 {
        let mut count = 0;

        count += feed_mappings
            .iter()
            .filter_map(|m| {
                if &m.category_id == category_id {
                    if pending_deleted_feeds.contains(&m.feed_id) {
                        return None;
                    }

                    item_count_map.get(&m.feed_id)
                } else {
                    None
                }
            })
            .sum::<i64>();

        count += category_mappings
            .iter()
            .filter_map(|m| {
                if &m.parent_id == category_id {
                    if pending_deleted_categories.contains(&m.category_id) {
                        return None;
                    }

                    Some(Self::calculate_item_count_for_category(
                        &m.category_id,
                        feed_mappings,
                        category_mappings,
                        item_count_map,
                        pending_deleted_feeds,
                        pending_deleted_categories,
                    ))
                } else {
                    None
                }
            })
            .sum::<i64>();

        count
    }

    fn create_mappings_for_uncategorized_feeds(feeds: &[Feed], mappings: &[FeedMapping]) -> Vec<FeedMapping> {
        let mut uncategorized_mappings = Vec::new();
        for (i, feed) in feeds.iter().enumerate() {
            if !mappings.iter().any(|m| m.feed_id == feed.feed_id) {
                uncategorized_mappings.push(FeedMapping {
                    feed_id: feed.feed_id.clone(),
                    category_id: NEWSFLASH_TOPLEVEL.clone(),
                    sort_index: Some(i as i32),
                });
            }
        }

        uncategorized_mappings
    }
}

impl Default for SidebarLoader {
    fn default() -> Self {
        Self {
            hide_future_articles: false,
            article_list_mode: ArticleListMode::All,
            undo_delete_actions: Vec::new(),
        }
    }
}
