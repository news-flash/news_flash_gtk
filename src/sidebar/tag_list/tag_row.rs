use crate::app::App;
use crate::i18n::i18n;
use crate::sidebar::tag_list::models::TagGObject;
use crate::util::{constants, GtkUtil};
use gdk4::Rectangle;
use gio::{Menu, MenuItem};
use glib::{clone, subclass::*, ParamSpec, ParamSpecString, SignalHandlerId, Value};
use gtk4::{prelude::*, subclass::prelude::*, CompositeTemplate, DrawingArea, Label};
use gtk4::{Align, GestureClick, GestureLongPress, PopoverMenu, PositionType};
use news_flash::models::{PluginCapabilities, TagID};
use once_cell::sync::Lazy;
use std::cell::RefCell;
use std::rc::Rc;
use std::str;

static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| {
    vec![Signal::builder("activated")
        .param_types([TagRow::static_type()])
        .build()]
});

mod imp {
    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/sidebar/tag_row.blp")]
    pub struct TagRow {
        pub id: RefCell<TagID>,
        pub color: Rc<RefCell<String>>,
        pub popover: RefCell<Option<PopoverMenu>>,
        pub right_click_source: RefCell<Option<SignalHandlerId>>,
        pub long_press_source: RefCell<Option<SignalHandlerId>>,
        pub activate_source: RefCell<Option<SignalHandlerId>>,

        #[template_child]
        pub tag_title: TemplateChild<Label>,
        #[template_child]
        pub tag_color: TemplateChild<DrawingArea>,
        #[template_child]
        pub row_click: TemplateChild<GestureClick>,
        #[template_child]
        pub row_activate: TemplateChild<GestureClick>,
        #[template_child]
        pub row_long_press: TemplateChild<GestureLongPress>,
    }

    impl Default for TagRow {
        fn default() -> Self {
            TagRow {
                id: RefCell::new(TagID::new("")),
                color: Rc::new(RefCell::new(constants::TAG_DEFAULT_COLOR.into())),
                popover: RefCell::new(None),
                right_click_source: RefCell::new(None),
                long_press_source: RefCell::new(None),
                activate_source: RefCell::new(None),

                tag_title: TemplateChild::default(),
                tag_color: TemplateChild::default(),
                row_click: TemplateChild::default(),
                row_activate: TemplateChild::default(),
                row_long_press: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TagRow {
        const NAME: &'static str = "TagRow";
        type ParentType = gtk4::Box;
        type Type = super::TagRow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for TagRow {
        fn signals() -> &'static [Signal] {
            SIGNALS.as_ref()
        }

        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecString::builder("title").build(),
                    ParamSpecString::builder("color").build(),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "title" => {
                    let input: String = value.get().expect("The value needs to be of type `string`.");
                    self.tag_title.set_label(&input);
                }
                "color" => {
                    let input: String = value.get().expect("The value needs to be of type `string`.");
                    self.color.replace(input);
                    self.tag_color.queue_draw();
                }
                _ => unreachable!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "title" => self.obj().get_title().to_value(),
                "color" => self.color.borrow().clone().to_value(),
                _ => unreachable!(),
            }
        }

        fn constructed(&self) {
            self.obj().init();
        }
    }

    impl WidgetImpl for TagRow {}

    impl BoxImpl for TagRow {}
}

glib::wrapper! {
    pub struct TagRow(ObjectSubclass<imp::TagRow>)
        @extends gtk4::Widget, gtk4::Box;
}

impl Default for TagRow {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl TagRow {
    pub fn new() -> Self {
        Self::default()
    }

    fn init(&self) {
        let imp = self.imp();
        imp.activate_source
            .replace(Some(imp.row_activate.connect_released(clone!(
                #[weak(rename_to = this)]
                self,
                #[upgrade_or_panic]
                move |_gesture, times, _x, _y| {
                    if times != 1 {
                        return;
                    }

                    this.activate();
                }
            ))));
    }

    pub fn bind_model(&self, model: &TagGObject) {
        let imp = self.imp();
        let is_same_tag = *imp.id.borrow() == model.tag_id();

        let support_mutation = App::default().features().contains(PluginCapabilities::SUPPORT_TAGS);

        imp.id.replace(model.tag_id());
        imp.color.replace(model.color());

        if support_mutation {
            self.setup_right_click();
        }

        imp.tag_color.set_draw_func(clone!(
            #[strong(rename_to = color)]
            imp.color,
            move |_drawing_area, ctx, _width, _height| {
                GtkUtil::draw_color_cirlce(ctx, &color.borrow(), None);
            }
        ));

        if !is_same_tag {
            imp.tag_title.set_label(&model.title());
        }
    }

    pub fn unbind_model(&self) {
        let imp = self.imp();

        GtkUtil::disconnect_signal(imp.right_click_source.take(), &*imp.row_click);
        GtkUtil::disconnect_signal(imp.long_press_source.take(), &*imp.row_long_press);
    }

    pub fn teardown_row(&self) {
        let imp = self.imp();

        GtkUtil::disconnect_signal(imp.activate_source.take(), &*imp.row_activate);

        _ = imp.popover.take();
    }

    fn get_title(&self) -> String {
        self.imp().tag_title.text().as_str().to_string()
    }

    fn setup_right_click(&self) {
        let imp = self.imp();

        imp.right_click_source
            .replace(Some(imp.row_click.connect_released(clone!(
                #[weak(rename_to = this)]
                self,
                #[upgrade_or_panic]
                move |_gesture, times, x, y| {
                    if times != 1 {
                        return;
                    }

                    if App::default().content_page_state().borrow().get_offline() {
                        return;
                    }

                    let imp = this.imp();
                    if imp.popover.borrow().is_none() {
                        imp.popover.replace(Some(this.setup_context_popover()));
                    }

                    if let Some(popover) = imp.popover.borrow().as_ref() {
                        popover.set_pointing_to(Some(&Rectangle::new(x as i32, y as i32, 0, 0)));
                        popover.popup();
                    };
                }
            ))));

        imp.long_press_source
            .replace(Some(imp.row_long_press.connect_pressed(clone!(
                #[weak(rename_to = this)]
                self,
                #[upgrade_or_panic]
                move |_gesture, x, y| {
                    if App::default().content_page_state().borrow().get_offline() {
                        return;
                    }

                    let imp = this.imp();
                    if imp.popover.borrow().is_none() {
                        imp.popover.replace(Some(this.setup_context_popover()));
                    }

                    if let Some(popover) = imp.popover.borrow().as_ref() {
                        popover.set_pointing_to(Some(&Rectangle::new(x as i32, y as i32, 0, 0)));
                        popover.popup();
                    };
                }
            ))));
    }

    fn setup_context_popover(&self) -> PopoverMenu {
        let tag_id = self.imp().id.borrow().clone();

        let model = Menu::new();
        let mark_read_item = MenuItem::new(Some(&i18n("Set Read")), None);
        let edit_item = MenuItem::new(Some(&i18n("Edit")), None);
        let delete_item = MenuItem::new(Some(&i18n("Delete")), None);

        let variant = tag_id.as_str().to_variant();
        mark_read_item.set_action_and_target_value(Some("win.mark-tag-read"), Some(&variant));
        edit_item.set_action_and_target_value(Some("win.edit-tag-dialog"), Some(&variant));

        let tuple_variant = (tag_id.as_str().to_string(), self.get_title()).to_variant();
        delete_item.set_action_and_target_value(Some("win.enqueue-delete-tag"), Some(&tuple_variant));

        model.append_item(&mark_read_item);
        model.append_item(&edit_item);
        model.append_item(&delete_item);

        let popover = PopoverMenu::from_model(Some(&model));
        popover.set_parent(self);
        popover.set_position(PositionType::Bottom);
        popover.set_has_arrow(false);
        popover.set_halign(Align::Start);
        popover
    }

    fn activate(&self) {
        self.emit_by_name::<()>("activated", &[&self.clone()]);
    }
}
