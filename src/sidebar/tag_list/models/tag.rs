use diffus::{Diffable, Same};
use news_flash::models::{Tag, TagID};
use std::cmp::Ordering;
use std::sync::Arc;

use crate::app::App;
use crate::settings::GFeedOrder;

#[derive(Eq, Clone, Debug)]
pub struct TagListTagModel {
    pub id: TagID,
    pub label: Arc<String>,
    pub color: Option<Arc<String>>,
    pub sort_index: i32,
}

impl TagListTagModel {
    pub fn new(tag: &Tag) -> Self {
        TagListTagModel {
            id: tag.tag_id.clone(),
            label: Arc::new(tag.label.clone()),
            color: tag.color.clone().map(Arc::new),
            sort_index: match tag.sort_index {
                Some(index) => index,
                None => i32::MAX,
            },
        }
    }
}

impl PartialEq for TagListTagModel {
    fn eq(&self, other: &TagListTagModel) -> bool {
        self.id == other.id
    }
}

impl Ord for TagListTagModel {
    fn cmp(&self, other: &TagListTagModel) -> Ordering {
        let order = App::default().settings().read().get_feed_list_order();
        match order {
            GFeedOrder::Alphabetical => self.label.to_lowercase().cmp(&other.label.to_lowercase()),
            GFeedOrder::Manual => self.sort_index.cmp(&other.sort_index),
        }
    }
}

impl PartialOrd for TagListTagModel {
    fn partial_cmp(&self, other: &TagListTagModel) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Same for TagListTagModel {
    fn same(&self, other: &Self) -> bool {
        self.id == other.id && self.label == other.label
    }
}

impl<'a> Diffable<'a> for TagListTagModel {
    type Diff = TagDiff;

    fn diff(&'a self, other: &'a Self) -> diffus::edit::Edit<Self> {
        let label = if self.label == other.label {
            None
        } else {
            Some(other.label.clone())
        };

        let color = if self.color == other.color {
            None
        } else {
            other.color.clone()
        };

        if self == other && label.is_none() && color.is_none() {
            diffus::edit::Edit::Copy(self)
        } else {
            diffus::edit::Edit::Change(TagDiff {
                id: self.id.clone(),
                label,
                color,
            })
        }
    }
}

pub struct TagDiff {
    pub id: TagID,
    pub label: Option<Arc<String>>,
    pub color: Option<Arc<String>>,
}
