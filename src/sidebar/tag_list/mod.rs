pub mod models;
mod tag_row;

use crate::color::ColorRGBA;
use crate::sidebar::SidebarIterateItem;
use crate::util::{constants, GtkUtil};
use diffus::edit::{collection, Edit};
use gdk4::Display;
use gio::ListStore;
use glib::clone;
use gtk4::{
    prelude::*, subclass::prelude::*, Box, CompositeTemplate, CssProvider, ListView, SignalListItemFactory,
    SingleSelection, Widget,
};
use gtk4::{ConstantExpression, ListItem, PropertyExpression, STYLE_PROVIDER_PRIORITY_APPLICATION};
use models::{TagGObject, TagListModel};
use news_flash::models::TagID;
use std::cell::RefCell;
use std::collections::HashMap;
use tag_row::TagRow;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/sidebar/tag_list.blp")]
    pub struct TagList {
        #[template_child]
        pub listview: TemplateChild<ListView>,
        #[template_child]
        pub factory: TemplateChild<SignalListItemFactory>,
        #[template_child]
        pub selection: TemplateChild<SingleSelection>,
        #[template_child]
        pub list_store: TemplateChild<ListStore>,

        pub list_model: RefCell<TagListModel>,
        pub model_index: RefCell<HashMap<TagID, TagGObject>>,
        pub style_provider: RefCell<CssProvider>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TagList {
        const NAME: &'static str = "TagList";
        type ParentType = Box;
        type Type = super::TagList;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for TagList {
        fn constructed(&self) {
            self.obj().init();
        }
    }

    impl WidgetImpl for TagList {}

    impl BoxImpl for TagList {}
}

glib::wrapper! {
    pub struct TagList(ObjectSubclass<imp::TagList>)
        @extends Widget, Box;
}

impl Default for TagList {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl TagList {
    pub fn new() -> Self {
        Self::default()
    }

    fn init(&self) {
        let imp = self.imp();

        imp.factory.connect_setup(clone!(
            #[weak(rename_to = this)]
            self,
            #[upgrade_or_panic]
            move |_factory, list_item| {
                let row = TagRow::new();

                let list_item = list_item.downcast_ref::<ListItem>().unwrap();
                list_item.set_child(Some(&row));

                row.connect_local(
                    "activated",
                    false,
                    clone!(
                        #[weak]
                        this,
                        #[upgrade_or_panic]
                        move |_args| {
                            let imp = this.imp();
                            imp.listview
                                .emit_by_name::<()>("activate", &[&imp.selection.selected()]);
                            None
                        }
                    ),
                );

                row.connect_destroy(|row| row.teardown_row());

                // Create expression describing `list_item->item`
                let list_item_expression = ConstantExpression::new(list_item);
                let article_gobject_expression =
                    PropertyExpression::new(ListItem::static_type(), Some(&list_item_expression), "item");

                // Update title
                let title_expression =
                    PropertyExpression::new(TagGObject::static_type(), Some(&article_gobject_expression), "title");
                title_expression.bind(&row, "title", Some(&row));

                // Update color
                let color_expression =
                    PropertyExpression::new(TagGObject::static_type(), Some(&article_gobject_expression), "color");
                color_expression.bind(&row, "color", Some(&row));
            }
        ));
        imp.factory.connect_bind(|_factory, list_item| {
            let list_item = list_item.downcast_ref::<ListItem>().unwrap();
            let tag = list_item.item().and_downcast::<TagGObject>().unwrap();
            let child = list_item.child().and_downcast::<TagRow>().unwrap();
            child.bind_model(&tag);
        });

        imp.factory.connect_unbind(|_factory, list_item| {
            let list_item = list_item.downcast_ref::<ListItem>().unwrap();
            let child = list_item.child().and_downcast::<TagRow>().unwrap();
            child.unbind_model();
        });
    }

    fn is_empty(&self) -> bool {
        let imp = self.imp();
        imp.list_store.n_items() == 0
    }

    pub fn listview(&self) -> &ListView {
        let imp = self.imp();
        &imp.listview
    }

    pub fn update(&self, new_list: TagListModel) {
        let imp = self.imp();

        self.update_style_provider(&new_list);

        let mut old_list = new_list;
        std::mem::swap(&mut old_list, &mut imp.list_model.borrow_mut());

        old_list.sort();
        imp.list_model.borrow_mut().sort();

        let model_guard = imp.list_model.borrow();
        let diff = old_list.generate_diff(&model_guard);
        let mut pos = 0;

        match diff {
            Edit::Copy(_list) => {
                // no difference
            }
            Edit::Change(diff) => {
                let _ = diff
                    .into_iter()
                    .map(|edit| {
                        match edit {
                            collection::Edit::Copy(_tag) => {
                                // nothing changed
                                pos += 1;
                            }
                            collection::Edit::Insert(tag) => {
                                let tag_id = tag.id.clone();
                                let tag_gobject = TagGObject::from_model(tag);
                                imp.list_store.insert(pos as u32, &tag_gobject);
                                imp.model_index.borrow_mut().insert(tag_id, tag_gobject);
                                pos += 1;
                            }
                            collection::Edit::Remove(tag) => {
                                let remove_pos = imp
                                    .model_index
                                    .borrow()
                                    .get(&tag.id)
                                    .and_then(|tag_gobject| imp.list_store.find(tag_gobject));
                                if let Some(remove_pos) = remove_pos {
                                    imp.list_store.remove(remove_pos);
                                    imp.model_index.borrow_mut().remove(&tag.id);
                                }
                            }
                            collection::Edit::Change(diff) => {
                                if let Some(tag_gobject) = imp.model_index.borrow().get(&diff.id) {
                                    if let Some(label) = diff.label {
                                        tag_gobject.set_title(label);
                                    }
                                    if let Some(color) = diff.color {
                                        tag_gobject.set_color(Some(color));
                                    }
                                }
                                pos += 1;
                            }
                        }
                    })
                    .collect::<Vec<_>>();
            }
        };
    }

    pub fn selection(&self) -> &SingleSelection {
        let imp = self.imp();
        &imp.selection
    }

    pub fn get_selected_tag_model(&self) -> Option<TagGObject> {
        if !self.is_empty() {
            let imp = self.imp();
            let selected_row_pos = imp.selection.selected();
            imp.list_store
                .item(selected_row_pos)
                .and_then(|obj| obj.downcast::<TagGObject>().ok())
        } else {
            None
        }
    }

    pub fn get_selection(&self) -> Option<(TagID, String)> {
        self.get_selected_tag_model().map(|tag| (tag.tag_id(), tag.title()))
    }

    pub fn get_next_item(&self) -> SidebarIterateItem {
        let imp = self.imp();

        if let Some(tag) = self.get_selected_tag_model() {
            return imp.list_model.borrow().calculate_next_item(&tag.tag_id());
        }
        SidebarIterateItem::NothingSelected
    }

    pub fn get_prev_item(&self) -> SidebarIterateItem {
        let imp = self.imp();

        if let Some(tag) = self.get_selected_tag_model() {
            return imp.list_model.borrow().calculate_prev_item(&tag.tag_id());
        }
        SidebarIterateItem::NothingSelected
    }

    pub fn get_first_item(&self) -> Option<TagID> {
        let imp = self.imp();
        imp.list_model.borrow().first().map(|model| model.id)
    }

    pub fn get_last_item(&self) -> Option<TagID> {
        let imp = self.imp();
        imp.list_model.borrow().last().map(|model| model.id)
    }

    pub fn set_selection(&self, selection: TagID) {
        let imp = self.imp();
        if let Some(tag_gobject) = imp.model_index.borrow().get(&selection) {
            if let Some(pos) = imp.list_store.find(tag_gobject) {
                imp.selection.set_selected(pos);
            }
        }
    }

    fn update_style_provider(&self, list_model: &TagListModel) {
        let imp = self.imp();
        gtk4::style_context_remove_provider_for_display(&Display::default().unwrap(), &*imp.style_provider.borrow());

        let mut css_string = String::new();
        for tag_model in list_model.tags() {
            let css_selector = tag_model
                .id
                .as_str()
                .chars()
                .filter(|c| c.is_alphanumeric() && !c.is_whitespace())
                .collect::<String>();
            let css_selector = format!("tag-style-{css_selector}");

            css_string.push_str(&Self::generate_css(
                &css_selector,
                tag_model.color.as_deref().map(|x| x.as_str()),
            ));
        }

        let provider = CssProvider::new();
        provider.load_from_string(&css_string);

        gtk4::style_context_add_provider_for_display(
            &Display::default().unwrap(),
            &provider,
            STYLE_PROVIDER_PRIORITY_APPLICATION,
        );
        imp.style_provider.replace(provider);
    }

    fn generate_css(tag_id: &str, color_str: Option<&str>) -> String {
        let tag_color = match ColorRGBA::parse_string(color_str.unwrap_or(constants::TAG_DEFAULT_COLOR)) {
            Ok(color) => color,
            Err(_) => ColorRGBA::parse_string(constants::TAG_DEFAULT_COLOR)
                .expect("Failed to parse default outer RGBA string."),
        };
        let gradient_upper = GtkUtil::adjust_lightness(&tag_color, constants::TAG_GRADIENT_SHIFT, None);
        let gradient_lower = GtkUtil::adjust_lightness(&tag_color, -constants::TAG_GRADIENT_SHIFT, None);

        let gradient_upper_hex = gradient_upper.as_string_no_alpha();
        let gradient_lower_hex = gradient_lower.as_string_no_alpha();

        let background_luminance = tag_color.luminance_normalized();
        let text_shift = if background_luminance > 0.8 {
            background_luminance - 1.3
        } else {
            1.2 - background_luminance
        };
        let mut text_color = tag_color;
        text_color.adjust_lightness_absolute(text_shift).unwrap();
        text_color
            .adjust_saturation_absolute(constants::TAG_TEXT_SATURATION_SHIFT)
            .unwrap();
        let text_color_hex = text_color.as_string_no_alpha();

        let css_str = format!(
            ".{} {{
                background-image: linear-gradient({}, {});
                color: {};
                border-radius: 12px;
                min-width: 12px;
                padding: 2px 5px 1px;
                opacity: 1.0;
            }}
            
            ",
            tag_id, gradient_upper_hex, gradient_lower_hex, text_color_hex
        );
        css_str
    }
}
