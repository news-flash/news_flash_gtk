use gdk4::Texture;
use glib::subclass;
use gtk4::prelude::*;
use gtk4::{subclass::prelude::*, Box, CompositeTemplate, Image, Label, Widget};
use std::str;

use crate::{app::App, sidebar::FeedListItemID};

mod imp {
    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/sidebar/feedlist_dnd_icon.blp")]
    #[derive(Default)]
    pub struct ItemRowDnD {
        #[template_child]
        pub item_count_label: TemplateChild<Label>,
        #[template_child]
        pub title: TemplateChild<Label>,
        #[template_child]
        pub favicon: TemplateChild<Image>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ItemRowDnD {
        const NAME: &'static str = "ItemRowDnD";
        type ParentType = Box;
        type Type = super::ItemRowDnD;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ItemRowDnD {}

    impl WidgetImpl for ItemRowDnD {}

    impl BoxImpl for ItemRowDnD {}
}

glib::wrapper! {
    pub struct ItemRowDnD(ObjectSubclass<imp::ItemRowDnD>)
        @extends Widget, Box;
}

impl Default for ItemRowDnD {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl ItemRowDnD {
    pub fn new(id: FeedListItemID, title: &str, count: u32) -> Self {
        let widget = Self::default();
        let imp = widget.imp();

        // Favicon
        if let FeedListItemID::Feed(feed_mapping) = &id {
            App::default().load_favicon(&feed_mapping.feed_id, &imp.favicon);
        } else {
            imp.favicon.set_paintable(None::<&Texture>);
            imp.favicon.set_visible(false);
        }

        // Title
        imp.title.set_text(title);

        // Item Count
        if count > 0 {
            imp.item_count_label.set_label(&count.to_string());
            imp.item_count_label.set_visible(true);
        } else {
            imp.item_count_label.set_visible(false);
        }

        widget
    }
}
