use crate::app::App;
use crate::error::NewsFlashGtkError;
use crate::i18n::i18n;
use crate::util::GtkUtil;
use eyre::{eyre, Result};
use glib::{clone, subclass, SignalHandlerId};
use gtk4::{prelude::*, subclass::prelude::*, Box, CompositeTemplate};
use libadwaita::{Toast, ToastOverlay};
use news_flash::error::NewsFlashError;
use news_flash::feed_api::FeedApiError;
use news_flash::models::{ApiSecret, LoginData, LoginGUI, OAuthData, PluginID, PluginInfo};
use std::cell::RefCell;
use std::rc::Rc;
use webkit6::Settings;
use webkit6::{prelude::WebViewExt, LoadEvent, WebView};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/login/web.blp")]
    pub struct WebLogin {
        #[template_child]
        pub toast_overlay: TemplateChild<ToastOverlay>,
        #[template_child]
        pub main_box: TemplateChild<Box>,

        pub redirect_signal_id: Rc<RefCell<Option<SignalHandlerId>>>,

        pub webview: Rc<RefCell<Option<WebView>>>,
        pub custom_api_secret: RefCell<Option<ApiSecret>>,
        pub plugin_id: RefCell<Option<PluginID>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for WebLogin {
        const NAME: &'static str = "WebLogin";
        type ParentType = gtk4::Box;
        type Type = super::WebLogin;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for WebLogin {
        fn constructed(&self) {
            self.obj().init();
        }
    }

    impl WidgetImpl for WebLogin {}

    impl BoxImpl for WebLogin {}
}

glib::wrapper! {
    pub struct WebLogin(ObjectSubclass<imp::WebLogin>)
        @extends gtk4::Widget, gtk4::Box;
}

impl Default for WebLogin {
    fn default() -> Self {
        Self::new()
    }
}

impl WebLogin {
    pub fn new() -> Self {
        glib::Object::new::<Self>()
    }

    fn init(&self) {
        let imp = self.imp();

        let settings = Settings::new();
        settings.set_enable_html5_database(false);
        settings.set_enable_html5_local_storage(false);
        settings.set_enable_page_cache(false);
        settings.set_enable_smooth_scrolling(true);
        settings.set_enable_javascript(true);
        settings.set_javascript_can_access_clipboard(false);
        settings.set_javascript_can_open_windows_automatically(false);
        settings.set_media_playback_requires_user_gesture(true);
        settings.set_user_agent_with_application_details(Some("Newsflash"), None);
        settings.set_enable_developer_extras(false);

        let webview = WebView::new();
        webview.set_settings(&settings);
        webview.set_hexpand(true);
        webview.set_vexpand(true);
        imp.main_box.append(&webview);

        imp.webview.replace(Some(webview));
    }

    pub fn set_custom_api_secret(&self, custom_api_secret: Option<&ApiSecret>) {
        self.imp().custom_api_secret.take();
        if let Some(custom_api_secret) = custom_api_secret {
            self.imp().custom_api_secret.replace(Some(custom_api_secret.clone()));
        }
    }

    pub fn show_error(&self, error: NewsFlashError) {
        let imp = self.imp();

        let limit_reached = i18n("API Limit Reached");
        let login_failed = i18n("Failed to log in");
        let unknown_error = i18n("Unknown error");

        let error_text = if let NewsFlashError::API(FeedApiError::ApiLimit) = error {
            limit_reached
        } else if let NewsFlashError::NotLoggedIn = error {
            login_failed.clone()
        } else {
            unknown_error
        };

        let toast = Toast::new(&error_text);
        toast.set_button_label(Some(&i18n("details")));
        toast.set_action_name(Some("win.show-error-dialog"));
        imp.toast_overlay.add_toast(toast);

        App::default().set_newsflash_error(NewsFlashGtkError::NewsFlash {
            source: error,
            context: login_failed,
        });
    }

    pub fn set_service(&self, info: &PluginInfo) -> Result<()> {
        let imp = self.imp();

        imp.plugin_id.replace(Some(info.id.clone()));

        let custom_api_secret = imp.custom_api_secret.borrow().clone();
        if let LoginGUI::OAuth(web_login_desc) = &info.login_gui {
            if let Some(url) = (web_login_desc.login_website)(custom_api_secret.as_ref()) {
                let webview = imp.webview.borrow().clone().ok_or(eyre!("no webview?"))?;

                webview.load_uri(url.as_str());
                let plugin_id = info.id.clone();
                let redirect_url = web_login_desc.catch_redirect.clone();
                let signal_id = webview.connect_load_changed(clone!(
                    #[weak(rename_to = redirect_signal_id)]
                    imp.redirect_signal_id,
                    #[strong]
                    custom_api_secret,
                    #[upgrade_or_panic]
                    move |webview, event| {
                        match event {
                            LoadEvent::Started | LoadEvent::Redirected => {
                                if let Some(redirect_url) = &redirect_url {
                                    if let Some(uri) = webview.uri() {
                                        if uri.len() > redirect_url.len() && &uri[..redirect_url.len()] == redirect_url
                                        {
                                            let oauth_data = OAuthData {
                                                id: plugin_id.clone(),
                                                url: uri.as_str().to_owned(),
                                                custom_api_secret: custom_api_secret.clone(),
                                            };
                                            let oauth_data = LoginData::OAuth(oauth_data);
                                            GtkUtil::disconnect_signal(redirect_signal_id.take(), webview);
                                            webview.stop_loading();

                                            App::default().login(oauth_data);
                                        }
                                    }
                                }
                            }
                            _ => {
                                // do nothing
                            }
                        }
                    }
                ));

                imp.redirect_signal_id.replace(Some(signal_id));
                return Ok(());
            }

            return Err(eyre!("No OAuth login URL"));
        }

        Err(eyre!("No login GUI description"))
    }

    pub fn reset(&self) {
        if let Some(webview) = self.imp().webview.borrow().as_ref() {
            webview.load_plain_text("")
        }
    }

    pub fn show(&self) {
        if let Some(webview) = self.imp().webview.borrow().as_ref() {
            webview.set_visible(true)
        }
    }
}
