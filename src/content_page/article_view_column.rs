use crate::app::App;
use crate::article_view::ArticleView;
use crate::enclosure_button::EnclosureButton;
use crate::i18n::i18n;
use crate::share::share_popover::SharePopover;
use crate::tag_popover::TagPopover;
use crate::util::constants;
use gio::{Menu, MenuItem, SimpleAction, SimpleActionGroup};
use glib::{clone, subclass, SignalHandlerId};
use gtk4::{
    prelude::*, subclass::prelude::*, CompositeTemplate, GestureClick, MenuButton, PositionType, Stack, ToggleButton,
};
use gtk4::{Button, Orientation, PopoverMenu};
use libadwaita::{HeaderBar, ToolbarView};
use news_flash::models::{ArticleID, Enclosure, Marked, PluginCapabilities, Read};
use std::cell::RefCell;
use std::rc::Rc;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/article_view/column.blp")]
    pub struct ArticleViewColumn {
        #[template_child]
        pub article_view: TemplateChild<ArticleView>,
        #[template_child]
        pub headerbar: TemplateChild<HeaderBar>,
        #[template_child]
        pub toolbar_view: TemplateChild<ToolbarView>,
        #[template_child]
        pub scrap_content_button: TemplateChild<ToggleButton>,
        #[template_child]
        pub footer_scrap_content_button: TemplateChild<ToggleButton>,
        #[template_child]
        pub enclosure_button: TemplateChild<EnclosureButton>,
        #[template_child]
        pub footer_enclosure_button: TemplateChild<EnclosureButton>,
        #[template_child]
        pub share_button: TemplateChild<MenuButton>,
        #[template_child]
        pub footer_share_button: TemplateChild<MenuButton>,
        #[template_child]
        pub scrap_content_stack: TemplateChild<Stack>,
        #[template_child]
        pub footer_scrap_content_stack: TemplateChild<Stack>,
        #[template_child]
        pub tag_button: TemplateChild<MenuButton>,
        #[template_child]
        pub tag_button_click: TemplateChild<GestureClick>,
        #[template_child]
        pub footer_tag_button: TemplateChild<MenuButton>,
        #[template_child]
        pub footer_tag_button_click: TemplateChild<GestureClick>,
        #[template_child]
        pub more_actions_button: TemplateChild<MenuButton>,
        #[template_child]
        pub more_actions_stack: TemplateChild<Stack>,
        #[template_child]
        pub mark_article_button: TemplateChild<ToggleButton>,
        #[template_child]
        pub mark_article_read_button: TemplateChild<ToggleButton>,
        #[template_child]
        pub mark_article_stack: TemplateChild<Stack>,
        #[template_child]
        pub mark_article_read_stack: TemplateChild<Stack>,

        pub tag_popover: TagPopover,
        pub footer_tag_popover: TagPopover,

        pub share_popover: SharePopover,
        pub footer_share_popover: SharePopover,

        pub scrap_content_event: Rc<RefCell<Option<SignalHandlerId>>>,
        pub footer_scrap_content_event: Rc<RefCell<Option<SignalHandlerId>>>,

        pub mark_article_event: Rc<RefCell<Option<SignalHandlerId>>>,
        pub mark_article_read_event: Rc<RefCell<Option<SignalHandlerId>>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ArticleViewColumn {
        const NAME: &'static str = "ArticleViewColumn";
        type ParentType = gtk4::Box;
        type Type = super::ArticleViewColumn;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ArticleViewColumn {
        fn constructed(&self) {
            self.obj().init();
        }
    }

    impl WidgetImpl for ArticleViewColumn {}

    impl BoxImpl for ArticleViewColumn {}
}

glib::wrapper! {
    pub struct ArticleViewColumn(ObjectSubclass<imp::ArticleViewColumn>)
        @extends gtk4::Widget, gtk4::Box;
}

impl Default for ArticleViewColumn {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl ArticleViewColumn {
    pub fn new() -> Self {
        Self::default()
    }

    fn init(&self) {
        let imp = self.imp();

        let zoom_in_action = SimpleAction::new("zoom-in", None);
        zoom_in_action.connect_activate(clone!(
            #[weak(rename_to = this)]
            self,
            #[upgrade_or_panic]
            move |_action, _parameter| {
                let view = this.imp().article_view.get();
                let zoom = view.property::<f64>("zoom");
                if zoom < constants::ARTICLE_ZOOM_UPPER {
                    view.set_property("zoom", zoom + 0.25);
                }
            }
        ));

        let zoom_out_action = SimpleAction::new("zoom-out", None);
        zoom_out_action.connect_activate(clone!(
            #[weak(rename_to = this)]
            self,
            #[upgrade_or_panic]
            move |_action, _parameter| {
                let view = this.imp().article_view.get();
                let zoom = view.property::<f64>("zoom");
                if zoom > constants::ARTICLE_ZOOM_LOWER {
                    view.set_property("zoom", zoom - 0.25);
                }
            }
        ));

        let reset_zoom_action = SimpleAction::new("reset-zoom", None);
        reset_zoom_action.connect_activate(clone!(
            #[weak(rename_to = this)]
            self,
            #[upgrade_or_panic]
            move |_action, _parameter| {
                this.imp().article_view.set_property("zoom", 1.0);
            }
        ));

        let action_group = SimpleActionGroup::new();
        action_group.add_action(&zoom_in_action);
        action_group.add_action(&zoom_out_action);
        action_group.add_action(&reset_zoom_action);
        self.insert_action_group("articleview", Some(&action_group));
        App::default().set_accels_for_action("articleview.zoom-in", &["<Ctrl>plus"]);
        App::default().set_accels_for_action("articleview.zoom-out", &["<Ctrl>minus"]);
        App::default().set_accels_for_action("articleview.reset-zoom", &["<Ctrl>0"]);

        self.setup_more_actions_button();
        self.update_share_popover_impl(false);
        Self::setup_scrap_content_button(
            &imp.scrap_content_event,
            &imp.footer_scrap_content_event,
            &imp.scrap_content_button,
            &imp.footer_scrap_content_button,
        );
        self.setup_mark_buttons();

        imp.tag_button.set_popover(Some(&imp.tag_popover));
        imp.footer_tag_button.set_popover(Some(&imp.footer_tag_popover));
        imp.tag_popover.set_position(PositionType::Bottom);
        imp.footer_tag_popover.set_position(PositionType::Top);

        imp.share_button.set_popover(Some(&imp.share_popover));
        imp.footer_share_button.set_popover(Some(&imp.footer_share_popover));
        imp.share_popover.set_position(PositionType::Bottom);
        imp.footer_share_popover.set_position(PositionType::Top);

        imp.enclosure_button.set_visible(false);
        imp.footer_enclosure_button.set_visible(false);
        imp.mark_article_button.set_sensitive(false);
        imp.mark_article_read_button.set_sensitive(false);
        imp.tag_button.set_sensitive(false);
        imp.footer_tag_button.set_sensitive(false);
        imp.scrap_content_button.set_sensitive(false);
        imp.footer_scrap_content_button.set_sensitive(false);
        imp.share_button.set_sensitive(false);
        imp.footer_share_button.set_sensitive(false);
    }

    fn setup_mark_buttons(&self) {
        let imp = self.imp();

        imp.mark_article_event
            .replace(Some(imp.mark_article_button.connect_toggled(clone!(
                #[weak(rename_to = column)]
                self,
                #[upgrade_or_panic]
                move |toggle_button| {
                    let imp = column.imp();

                    if toggle_button.is_active() {
                        imp.mark_article_stack.set_visible_child_name("marked");
                    } else {
                        imp.mark_article_stack.set_visible_child_name("unmarked");
                    }
                    App::default().toggle_selected_article_marked();
                }
            ))));

        imp.mark_article_read_event
            .replace(Some(imp.mark_article_read_button.connect_toggled(clone!(
                #[weak(rename_to = column)]
                self,
                #[upgrade_or_panic]
                move |toggle_button| {
                    let imp = column.imp();

                    if toggle_button.is_active() {
                        imp.mark_article_read_stack.set_visible_child_name("unread");
                    } else {
                        imp.mark_article_read_stack.set_visible_child_name("read");
                    }
                    App::default().toggle_selected_article_read();
                }
            ))));
    }

    fn setup_scrap_content_button(
        scrap_content_event: &Rc<RefCell<Option<SignalHandlerId>>>,
        footer_scrap_content_event: &Rc<RefCell<Option<SignalHandlerId>>>,
        scrap_content_button: &ToggleButton,
        footer_scrap_content_button: &ToggleButton,
    ) {
        scrap_content_event.replace(Some(scrap_content_button.connect_toggled(clone!(
            #[weak]
            footer_scrap_content_button,
            #[upgrade_or_panic]
            move |button| {
                footer_scrap_content_button.set_active(button.is_active());
                if button.is_active() {
                    App::default()
                        .content_page_state()
                        .borrow_mut()
                        .set_prefer_scraped_content(true);
                    ActionGroupExt::activate_action(&App::default().main_window(), "scrap-content", None);
                } else {
                    App::default()
                        .content_page_state()
                        .borrow_mut()
                        .set_prefer_scraped_content(false);
                    App::default()
                        .main_window()
                        .content_page()
                        .articleview_column()
                        .article_view()
                        .redraw_article();
                }
            }
        ))));

        footer_scrap_content_event.replace(Some(footer_scrap_content_button.connect_toggled(clone!(
            #[weak]
            scrap_content_button,
            #[upgrade_or_panic]
            move |button| {
                // only trigger the top button: this will in turn send the action to the app
                if scrap_content_button.is_active() != button.is_active() {
                    scrap_content_button.set_active(button.is_active());
                }
            }
        ))));
    }

    fn setup_more_actions_button(&self) {
        let imp = self.imp();

        let zoom_item = MenuItem::new(Some("zoom_widget"), None);
        zoom_item.set_attribute_value("custom", Some(&"zoom".to_variant()));

        let section = Menu::new();
        section.append(Some(&i18n("Export Article")), Some("win.export-article"));
        section.append(
            Some(&i18n("Open in Browser")),
            Some("win.open-selected-article-in-browser"),
        );
        section.append(Some(&i18n("Toggle Fullscreen")), Some("win.fullscreen-article"));
        section.append(Some(&i18n("Close Article")), Some("win.close-article"));

        let model = Menu::new();
        model.append_item(&zoom_item);
        model.append_section(None, &section);

        imp.more_actions_button.set_menu_model(Some(&model));

        if let Some(popover) = imp.more_actions_button.popover().and_downcast::<PopoverMenu>() {
            let article_view = imp.article_view.get();

            let zoom_plus = Button::builder()
                .icon_name("zoom-in-symbolic")
                .tooltip_text("Zoom In")
                .css_classes(vec!["circular", "flat"])
                .action_name("articleview.zoom-in")
                .build();
            let zoom_minus = Button::builder()
                .icon_name("zoom-out-symbolic")
                .tooltip_text("Zoom Out")
                .css_classes(vec!["circular", "flat"])
                .action_name("articleview.zoom-out")
                .build();
            let zoom_reset = Button::builder()
                .label("100%")
                .tooltip_text("Reset Zoom")
                .css_classes(vec!["flat"])
                .action_name("articleview.reset-zoom")
                .hexpand(true)
                .build();
            zoom_reset
                .bind_property("label", &article_view, "zoom")
                .bidirectional()
                .transform_from(|_binding, b: f64| Some(format!("{}%", b * 100.0)))
                .build();
            let zoom_box = gtk4::Box::builder()
                .spacing(12)
                .orientation(Orientation::Horizontal)
                .margin_start(18)
                .margin_end(18)
                .build();
            zoom_box.append(&zoom_minus);
            zoom_box.append(&zoom_reset);
            zoom_box.append(&zoom_plus);

            popover.add_child(&zoom_box, "zoom");
        }
    }

    pub fn article_view(&self) -> &ArticleView {
        let imp = self.imp();
        &imp.article_view
    }

    pub fn headerbar(&self) -> &HeaderBar {
        let imp = self.imp();
        &imp.headerbar
    }

    pub fn toolbar_view(&self) -> &ToolbarView {
        let imp = self.imp();
        &imp.toolbar_view
    }

    pub fn show_article(
        &self,
        article_id: &ArticleID,
        read: Read,
        marked: Marked,
        have_scraped_content: bool,
        enclosures: Option<&Vec<Enclosure>>,
    ) {
        let imp = self.imp();
        let content_page_state = App::default().content_page_state();
        let offline = content_page_state.borrow().get_offline();

        let (unread_icon, unread_active) = match read {
            Read::Read => ("read", false),
            Read::Unread => ("unread", true),
        };
        let (marked_icon, marked_active) = match marked {
            Marked::Marked => ("marked", true),
            Marked::Unmarked => ("unmarked", false),
        };

        imp.mark_article_stack.set_visible_child_name(marked_icon);
        imp.mark_article_read_stack.set_visible_child_name(unread_icon);

        // block signals
        if let Some(signal_id) = imp.mark_article_read_event.borrow().as_ref() {
            imp.mark_article_read_button.block_signal(signal_id);
        }
        if let Some(signal_id) = imp.mark_article_event.borrow().as_ref() {
            imp.mark_article_button.block_signal(signal_id);
        }

        // set state
        imp.mark_article_button.set_active(marked_active);
        imp.mark_article_read_button.set_active(unread_active);

        // unblock signals
        if let Some(signal_id) = imp.mark_article_read_event.borrow().as_ref() {
            imp.mark_article_read_button.unblock_signal(signal_id);
        }
        if let Some(signal_id) = imp.mark_article_event.borrow().as_ref() {
            imp.mark_article_button.unblock_signal(signal_id);
        }

        let scrap_ongoing = content_page_state.borrow().is_article_scrap_ongoing();

        if scrap_ongoing {
            self.start_scrap_content_spinner();
        } else {
            let show_scraped_content = have_scraped_content && content_page_state.borrow().get_prefer_scraped_content();
            self.stop_scrap_content_spinner();
            self.update_scrape_content_button_state(show_scraped_content);
        }

        if let Some(enclosures) = enclosures {
            imp.enclosure_button.update(enclosures);
            imp.footer_enclosure_button.update(enclosures);
        }

        imp.enclosure_button.set_visible(enclosures.is_some());
        imp.footer_enclosure_button.set_visible(enclosures.is_some());

        let tag_support = App::default().features().contains(PluginCapabilities::SUPPORT_TAGS);

        imp.mark_article_button.set_sensitive(true);
        imp.mark_article_read_button.set_sensitive(true);
        self.update_tags(Some(article_id));
        imp.tag_button.set_sensitive(tag_support);
        imp.footer_tag_button.set_sensitive(tag_support);

        imp.scrap_content_button.set_sensitive(!offline && !scrap_ongoing);
        imp.footer_scrap_content_button
            .set_sensitive(!offline && !scrap_ongoing);

        imp.share_button.set_sensitive(!offline);
        imp.footer_share_button.set_sensitive(!offline);
    }

    pub fn clear_article(&self) {
        let imp = self.imp();

        imp.mark_article_stack.set_visible_child_name("unmarked");
        imp.mark_article_read_stack.set_visible_child_name("read");
        imp.scrap_content_stack.set_visible_child_name("button");
        imp.footer_scrap_content_stack.set_visible_child_name("button");

        // block signals
        if let Some(signal_id) = imp.mark_article_read_event.borrow().as_ref() {
            imp.mark_article_read_button.block_signal(signal_id);
        }
        if let Some(signal_id) = imp.mark_article_event.borrow().as_ref() {
            imp.mark_article_button.block_signal(signal_id);
        }
        if let Some(signal_id) = imp.scrap_content_event.borrow().as_ref() {
            imp.scrap_content_button.block_signal(signal_id);
        }
        if let Some(signal_id) = imp.footer_scrap_content_event.borrow().as_ref() {
            imp.footer_scrap_content_button.block_signal(signal_id);
        }

        // set state
        imp.mark_article_button.set_active(false);
        imp.mark_article_read_button.set_active(false);
        imp.scrap_content_button.set_active(false);
        imp.footer_scrap_content_button.set_active(false);
        imp.mark_article_button.set_sensitive(false);
        imp.mark_article_read_button.set_sensitive(false);
        imp.scrap_content_button.set_sensitive(false);
        imp.footer_scrap_content_button.set_sensitive(false);

        // unblock signals
        if let Some(signal_id) = imp.mark_article_read_event.borrow().as_ref() {
            imp.mark_article_read_button.unblock_signal(signal_id);
        }
        if let Some(signal_id) = imp.mark_article_event.borrow().as_ref() {
            imp.mark_article_button.unblock_signal(signal_id);
        }
        if let Some(signal_id) = imp.scrap_content_event.borrow().as_ref() {
            imp.scrap_content_button.unblock_signal(signal_id);
        }
        if let Some(signal_id) = imp.footer_scrap_content_event.borrow().as_ref() {
            imp.footer_scrap_content_button.block_signal(signal_id);
        }

        imp.enclosure_button.set_visible(false);
        imp.footer_enclosure_button.set_visible(false);

        self.update_tags(None);
        imp.tag_button.set_sensitive(false);
        imp.footer_tag_button.set_sensitive(false);

        imp.share_button.set_sensitive(false);
        imp.footer_share_button.set_sensitive(false);

        imp.article_view.close_article();
    }

    pub fn update_enclosures(&self, enclosures: &[Enclosure]) {
        let imp = self.imp();
        imp.enclosure_button.update(enclosures);
        imp.footer_enclosure_button.update(enclosures);
    }

    fn update_scrape_content_button_state(&self, active: bool) {
        let imp = self.imp();

        // block signals
        if let Some(signal_id) = imp.scrap_content_event.borrow().as_ref() {
            imp.scrap_content_button.block_signal(signal_id);
        }
        if let Some(signal_id) = imp.footer_scrap_content_event.borrow().as_ref() {
            imp.footer_scrap_content_button.block_signal(signal_id);
        }

        // set state
        imp.scrap_content_button.set_active(active);
        imp.footer_scrap_content_button.set_active(active);

        // unblock signals
        if let Some(signal_id) = imp.scrap_content_event.borrow().as_ref() {
            imp.scrap_content_button.unblock_signal(signal_id);
        }
        if let Some(signal_id) = imp.footer_scrap_content_event.borrow().as_ref() {
            imp.footer_scrap_content_button.unblock_signal(signal_id);
        }
    }

    fn update_tags(&self, article_id: Option<&ArticleID>) {
        let imp = self.imp();

        if let Some(article_id) = article_id {
            imp.tag_popover.set_article_id(article_id);
            imp.footer_tag_popover.set_article_id(article_id);

            // update popovers when one of them is popped up or down
            // to keep footer & header popover in sync
            imp.tag_button_click.connect_pressed(clone!(
                #[weak(rename_to = column)]
                self,
                #[strong]
                article_id,
                #[upgrade_or_panic]
                move |_gesture_click, times, _x, _y| {
                    if times != 1 {
                        return;
                    }

                    let imp = column.imp();
                    imp.tag_popover.set_article_id(&article_id);
                }
            ));
            imp.footer_tag_button_click.connect_pressed(clone!(
                #[weak(rename_to = column)]
                self,
                #[strong]
                article_id,
                #[upgrade_or_panic]
                move |_gesture_click, times, _x, _y| {
                    if times != 1 {
                        return;
                    }

                    let imp = column.imp();
                    imp.footer_tag_popover.set_article_id(&article_id);
                }
            ));
        }
    }

    pub fn popup_tag_popover(&self) {
        let imp = self.imp();
        if imp.toolbar_view.reveals_bottom_bars() {
            imp.footer_tag_button.popup();
        } else {
            imp.tag_button.popup();
        }
    }

    pub fn start_scrap_content_spinner(&self) {
        let imp = self.imp();
        imp.scrap_content_button.set_sensitive(false);
        imp.footer_scrap_content_button.set_sensitive(false);
        imp.scrap_content_stack.set_visible_child_name("spinner");
        imp.footer_scrap_content_stack.set_visible_child_name("spinner");
    }

    pub fn stop_scrap_content_spinner(&self) {
        let imp = self.imp();
        let offline = App::default().content_page_state().borrow().get_offline();
        imp.scrap_content_button.set_sensitive(!offline);
        imp.footer_scrap_content_button.set_sensitive(!offline);

        // are we shoing scraped content right now?
        let scraped_content_visible = App::default()
            .main_window()
            .content_page()
            .visible_article_manager()
            .get_article()
            .map(|a| a.scraped_content.is_some())
            .unwrap_or(false);

        self.update_scrape_content_button_state(scraped_content_visible);

        imp.scrap_content_stack.set_visible_child_name("button");
        imp.footer_scrap_content_stack.set_visible_child_name("button");
    }

    pub fn start_more_actions_spinner(&self) {
        let imp = self.imp();
        imp.more_actions_button.set_sensitive(false);
        imp.more_actions_stack.set_visible_child_name("spinner");
    }

    pub fn stop_more_actions_spinner(&self) {
        let imp = self.imp();
        imp.more_actions_button.set_sensitive(true);
        imp.more_actions_stack.set_visible_child_name("button");
    }

    pub fn set_offline(&self, offline: bool) {
        let imp = self.imp();

        let have_visible_article = App::default()
            .main_window()
            .content_page()
            .visible_article_manager()
            .get_article()
            .is_some();
        let sensitive = have_visible_article && !offline;

        imp.scrap_content_button.set_sensitive(sensitive);
        imp.footer_scrap_content_button.set_sensitive(sensitive);
        imp.share_button.set_sensitive(sensitive);
        imp.footer_share_button.set_sensitive(sensitive);
    }

    pub fn update_share_popover(&self) {
        let have_visible_article = App::default()
            .main_window()
            .content_page()
            .visible_article_manager()
            .get_article()
            .is_some();
        let sensitive = have_visible_article && !App::default().content_page_state().borrow().get_offline();
        self.update_share_popover_impl(sensitive);
    }

    fn update_share_popover_impl(&self, sensitive: bool) {
        let imp = self.imp();
        imp.share_button.set_sensitive(sensitive);
        imp.footer_share_button.set_sensitive(sensitive);

        let show_button = App::default().settings().read().get_any_share_services_enabled();
        imp.share_button.set_visible(show_button);
        imp.footer_share_button.set_visible(show_button);

        imp.share_popover.update();
        imp.footer_share_popover.update();
    }
}
