use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Default, Debug, Serialize, PartialEq, Deserialize)]
pub enum ArticleListMode {
    #[default]
    All,
    Unread,
    Marked,
}
