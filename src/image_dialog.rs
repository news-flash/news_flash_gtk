use crate::app::App;
use crate::i18n::{i18n, i18n_f};
use base64::engine::general_purpose::STANDARD as base64_std;
use base64::Engine;
use bytesize::ByteSize;
use gdk4::Texture;
use glib::{clone, subclass};
use gtk4::{prelude::*, subclass::prelude::*, CompositeTemplate, Label, Picture, Stack, Widget};
use libadwaita::{prelude::*, subclass::prelude::*, Dialog};
use news_flash::{
    error::NewsFlashError,
    models::{ArticleID, Url},
    Progress,
};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/media/image_dialog.blp")]
    pub struct ImageDialog {
        #[template_child]
        pub picture: TemplateChild<Picture>,
        #[template_child]
        pub picture_stack: TemplateChild<Stack>,
        #[template_child]
        pub progress_label: TemplateChild<Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ImageDialog {
        const NAME: &'static str = "ImageDialog";
        type ParentType = Dialog;
        type Type = super::ImageDialog;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ImageDialog {}

    impl WidgetImpl for ImageDialog {}

    impl AdwDialogImpl for ImageDialog {}
}

glib::wrapper! {
    pub struct ImageDialog(ObjectSubclass<imp::ImageDialog>)
        @extends Widget, Dialog;
}

impl ImageDialog {
    pub fn new_url(article_id: &ArticleID, url: &Url) -> Self {
        let dialog = glib::Object::new::<Self>();

        let (sender, mut receiver) = tokio::sync::mpsc::channel::<Progress>(2);

        let imp = dialog.imp();
        let progress_label = imp.progress_label.clone();
        glib::MainContext::default().spawn_local(async move {
            while let Some(progress) = receiver.recv().await {
                let downloaded = ByteSize(progress.downloaded as u64);
                let total = ByteSize(progress.total_size as u64);
                let progress_text = format!("{downloaded} of {total}");
                if !progress_label.is_visible() {
                    progress_label.set_visible(true);
                }
                progress_label.set_text(&progress_text);
                log::debug!("{}", progress_text);
            }
        });

        let url = url.clone();
        let article_id = article_id.clone();
        App::default().execute_with_callback(
            |news_flash, client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash
                        .get_image(&article_id, url.as_str(), &client, Some(sender))
                        .await
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            clone!(
                #[weak]
                dialog,
                #[upgrade_or_panic]
                move |_app, res| {
                    match res {
                        Ok(res) => {
                            dialog.imp().picture_stack.set_visible_child_name("picture");

                            let bytes = glib::Bytes::from_owned(res);
                            let texture = match Texture::from_bytes(&bytes) {
                                Ok(texture) => texture,
                                Err(error) => {
                                    App::default().in_app_notifiaction(&i18n_f(
                                        "Failed to decode image '{}'",
                                        &[&error.to_string()],
                                    ));
                                    dialog.close();
                                    return;
                                }
                            };
                            dialog.imp().picture.set_paintable(Some(&texture));

                            dialog.set_content_width(texture.width());
                            dialog.set_content_height(texture.height());
                        }
                        Err(error) => {
                            dialog.close();
                            App::default().in_app_error(&i18n("Failed to download image"), error);
                        }
                    }
                }
            ),
        );

        dialog
    }

    pub fn new_base64_data(base64_data: &str) -> Option<Self> {
        if let Some(decoded_data) = base64_data
            .find(',')
            .and_then(|start| base64_std.decode(&base64_data[start + 1..]).ok())
        {
            let dialog = glib::Object::new::<Self>();

            dialog.imp().picture_stack.set_visible_child_name("picture");

            let bytes = glib::Bytes::from_owned(decoded_data);
            let texture = match Texture::from_bytes(&bytes) {
                Ok(texture) => texture,
                Err(error) => {
                    App::default().in_app_notifiaction(&i18n_f("Failed to decode image '{}'", &[&error.to_string()]));
                    return None;
                }
            };
            dialog.imp().picture.set_paintable(Some(&texture));

            dialog.set_content_width(texture.width());
            dialog.set_content_height(texture.height());
            Some(dialog)
        } else {
            App::default().in_app_notifiaction(&i18n("Could not decode image"));
            None
        }
    }
}
