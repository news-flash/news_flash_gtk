use crate::config::VERSION;
use gtk4::{prelude::*, Widget};
use libadwaita::prelude::*;
use libadwaita::AboutDialog;

pub const APP_NAME: &str = "Newsflash";
pub const COPYRIGHT: &str = "Copyright © 2017-2024 Jan Lukas Gernert";
pub const DESCRIPTION: &str = r#"<b>Follow your favorite blogs &amp; news sites.</b>

Newsflash is an application designed to complement an already existing web-based RSS reader account.

It combines all the advantages of web based services like syncing across all your devices with everything you expect from a modern desktop application.

<b>Features:</b>

 - Desktop notifications
 - Fast search and filtering
 - Tagging
 - Keyboard shortcuts
 - Offline support
 - and much more

<b>Supported Services:</b>

 - Miniflux
 - local RSS
 - fever
 - Fresh RSS
 - NewsBlur
 - Inoreader
 - feedbin
 - Nextcloud News"#;
pub const AUTHORS: &[&str] = &["Jan Lukas Gernert", "Felix Bühler", "Alistair Francis"];
pub const DESIGNERS: &[&str] = &["Jan Lukas Gernert"];
pub const ARTISTS: &[&str] = &["Tobias Bernard"];

#[derive(Clone, Debug)]
pub struct NewsFlashAbout;

impl NewsFlashAbout {
    pub fn show<W: IsA<Widget> + WidgetExt>(window: &W) {
        let dialog = AboutDialog::from_appdata(
            "/io/gitlab/news_flash/NewsFlash/io.gitlab.news_flash.NewsFlash.appdata.xml",
            Some(VERSION),
        );
        dialog.set_version(VERSION);
        dialog.set_developers(AUTHORS);
        dialog.set_designers(DESIGNERS);
        dialog.set_artists(ARTISTS);
        dialog.set_comments(DESCRIPTION);
        dialog.set_copyright(COPYRIGHT);
        dialog.present(Some(window))
    }
}
