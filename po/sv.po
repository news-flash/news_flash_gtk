# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the newsflash package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: newsflash\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-06-01 17:44+0200\n"
"PO-Revision-Date: 2024-07-05 08:53+0000\n"
"Last-Translator: Jesper <93771679+Bjorkan@users.noreply.github.com>\n"
"Language-Team: Swedish <https://hosted.weblate.org/projects/newsflash/"
"news_flash_gtk/sv/>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.7-dev\n"

#: data/io.gitlab.news_flash.NewsFlash.appdata.xml.in.in:7
msgid "NewsFlash"
msgstr "NewsFlash"

#: data/io.gitlab.news_flash.NewsFlash.appdata.xml.in.in:8
msgid "Keep up with your feeds"
msgstr "Håll dig uppdaterad på dina feeds"

#: data/io.gitlab.news_flash.NewsFlash.appdata.xml.in.in:12
msgid ""
"NewsFlash is a program designed to complement an already existing web-based "
"RSS reader account."
msgstr ""
"NewsFlash är ett program som är utformat för att komplettera ett redan "
"existerande webbaserat RSS-läsarkonto."

#: data/io.gitlab.news_flash.NewsFlash.appdata.xml.in.in:16
msgid ""
"It combines all the advantages of web based services like syncing across all "
"your devices with everything you expect from a modern desktop program: "
"Desktop notifications, fast search and filtering, tagging, handy keyboard "
"shortcuts and having access to all your articles for as long as you like."
msgstr ""
"Det kombinerar alla fördelarna med webbaserade tjänster som att synkronisera "
"över alla dina enheter med allt du förväntar dig från ett modernt "
"skrivbordsprogram: Skrivbordsmeddelanden, snabbsökning och filtrering, "
"tagging, praktiska tangentbordsgenvägar och har tillgång till alla dina "
"artiklar så länge du vill."

#: data/io.gitlab.news_flash.NewsFlash.appdata.xml.in.in:24
msgid "Main Window"
msgstr "Huvudfönster"

#: data/io.gitlab.news_flash.NewsFlash.appdata.xml.in.in:28
msgid "Main Window Dark"
msgstr "Huvudfönster mörk"

#: data/io.gitlab.news_flash.NewsFlash.appdata.xml.in.in:32
#: src/content_page/sidebar_column.rs:125
msgid "Preferences"
msgstr "Preferenser"

#: data/io.gitlab.news_flash.NewsFlash.appdata.xml.in.in:36
msgid "Adaptive Main Window"
msgstr "Adaptivt huvudfönster"

#: data/io.gitlab.news_flash.NewsFlash.desktop.in.in:3
#: data/resources/ui_templates/main_window.blp:6
#: data/resources/ui_templates/main_window.blp:14
#: data/resources/ui_templates/main_window.blp:42
msgid "Newsflash"
msgstr "Newsflash"

#: data/io.gitlab.news_flash.NewsFlash.desktop.in.in:4
msgid "RSS Reader"
msgstr "RSS Läsare"

#: data/io.gitlab.news_flash.NewsFlash.desktop.in.in:5
msgid ""
"Faster than flash to deliver you the latest news from your subscribed feeds"
msgstr ""
"Snabbare än blixten att leverera de senaste nyheterna från dina "
"prenumererade flöden"

#: data/io.gitlab.news_flash.NewsFlash.desktop.in.in:11
msgid "Gnome;GTK;RSS;Feed;"
msgstr "Gnome;GTK;RSS;Flöde;"

#: data/resources/ui_templates/add_dialog/category.blp:39
msgid "Add a Category"
msgstr "Lägga till en kategori"

#: data/resources/ui_templates/add_dialog/category.blp:50
msgid "Enter the Title of the new Category"
msgstr "Ange titeln på den nya kategorin"

#: data/resources/ui_templates/add_dialog/category.blp:61
msgid "Category Title"
msgstr "Kategori titel"

#: data/resources/ui_templates/add_dialog/category.blp:67
#: data/resources/ui_templates/add_dialog/feed_widget.blp:70
#: data/resources/ui_templates/add_dialog/tag.blp:67
msgid "Add"
msgstr "Lägg till"

#: data/resources/ui_templates/add_dialog/error.blp:35
msgid "Try Again"
msgstr "Försök igen"

#: data/resources/ui_templates/add_dialog/feed.blp:12
msgid "Parse URL"
msgstr "Bearbeta URL"

#: data/resources/ui_templates/add_dialog/feed.blp:18
#: data/resources/ui_templates/discover/dialog.blp:314
msgid "Select Feed"
msgstr "Välj flöde"

#: data/resources/ui_templates/add_dialog/feed.blp:24
#: data/resources/ui_templates/add_dialog/feed_widget.blp:31
#: data/resources/ui_templates/discover/dialog.blp:320
msgid "Add Feed"
msgstr "Lägg till flöde"

#: data/resources/ui_templates/add_dialog/feed.blp:30
msgid "Error"
msgstr "Fel"

#: data/resources/ui_templates/add_dialog/feed_widget.blp:40
msgid "Edit the title of the feed and set its category"
msgstr "Redigera titeln på flödet och ange dess kategori"

#: data/resources/ui_templates/add_dialog/feed_widget.blp:61
#: data/resources/ui_templates/sidebar/feedlist_dnd_icon.blp:29
#: data/resources/ui_templates/sidebar/feedlist_item.blp:29
msgid "Feed Title"
msgstr "Flödets titel"

#: data/resources/ui_templates/add_dialog/feed_widget.blp:64
#: data/resources/ui_templates/edit_dialogs/feed.blp:51
#: src/content_page/sidebar_column.rs:147
msgid "Category"
msgstr "Kategori"

#: data/resources/ui_templates/add_dialog/parse_feed.blp:39
msgid "Add a Feed"
msgstr "Lägg till flöde"

#: data/resources/ui_templates/add_dialog/parse_feed.blp:50
msgid "Enter the URL of a feed or website"
msgstr "Ange webbadressen till ett flöde eller en webbplats"

#: data/resources/ui_templates/add_dialog/parse_feed.blp:61
msgid "Website or Feed URL"
msgstr "Webbplats eller flöde URL"

#: data/resources/ui_templates/add_dialog/parse_feed.blp:68
msgid "Parse"
msgstr "Bearbeta"

#: data/resources/ui_templates/add_dialog/select_feed.blp:32
msgid "Multiple Feeds Found"
msgstr "Flera flöden hittades"

#: data/resources/ui_templates/add_dialog/select_feed.blp:43
msgid "Select one of the listed feeds below"
msgstr "Välj ett av de listade flödena nedan"

#: data/resources/ui_templates/add_dialog/select_feed.blp:69
msgid "Select"
msgstr "Välj"

#: data/resources/ui_templates/add_dialog/tag.blp:39
msgid "Add a Tag"
msgstr "Lägg till en tagg"

#: data/resources/ui_templates/add_dialog/tag.blp:50
msgid "Enter the Title of the new Tag"
msgstr "Ange titeln på den nya taggen"

#: data/resources/ui_templates/add_dialog/tag.blp:61
#: data/resources/ui_templates/sidebar/tag_row.blp:25
#: data/resources/ui_templates/tagging/row.blp:26
msgid "Tag Title"
msgstr "Taggtitel"

#: data/resources/ui_templates/article_list/column.blp:27
msgid "Refresh Content"
msgstr "Uppdatera innehållet"

#: data/resources/ui_templates/article_list/column.blp:45
msgid "Search Articles"
msgstr "Sök artiklar"

#: data/resources/ui_templates/article_list/column.blp:58
msgid "Mark List as Read"
msgstr "Markera lista som läst"

#: data/resources/ui_templates/article_list/column.blp:96
msgid "All"
msgstr "Alla"

#: data/resources/ui_templates/article_list/column.blp:103
msgid "Unread"
msgstr "Olästa"

#: data/resources/ui_templates/article_list/column.blp:110
msgid "Starred"
msgstr "Stjärnmarkerad"

#: data/resources/ui_templates/article_list/list.blp:19
#: src/article_list/mod.rs:653
msgid "No Articles"
msgstr "Inga artiklar"

#: data/resources/ui_templates/article_list/row.blp:43
msgid "feed"
msgstr "flöde"

#: data/resources/ui_templates/article_list/row.blp:58
msgid "/"
msgstr "/"

#: data/resources/ui_templates/article_list/row.blp:72
msgid "date"
msgstr "datum"

#: data/resources/ui_templates/article_list/row.blp:100
#: data/resources/ui_templates/article_list/row.blp:101
msgid "title"
msgstr "titel"

#: data/resources/ui_templates/article_list/row.blp:115
msgid "No Summary"
msgstr "Ingen sammanfattning"

#: data/resources/ui_templates/article_view/column.blp:21
#: data/resources/ui_templates/settings/shortcuts.blp:63
msgid "Toggle Starred"
msgstr "Växla stjärnmarkerade"

#: data/resources/ui_templates/article_view/column.blp:29
msgid "unmarked"
msgstr "omärkta"

#: data/resources/ui_templates/article_view/column.blp:37
msgid "marked"
msgstr "markerade"

#: data/resources/ui_templates/article_view/column.blp:52
#: data/resources/ui_templates/settings/shortcuts.blp:46
#: src/article_list/article_row.rs:426
msgid "Toggle Read"
msgstr "Växla lästa"

#: data/resources/ui_templates/article_view/column.blp:60
msgid "read"
msgstr "läst"

#: data/resources/ui_templates/article_view/column.blp:68
msgid "unread"
msgstr "oläst"

#: data/resources/ui_templates/article_view/column.blp:82
msgid "Article Menu"
msgstr "Artikelmeny"

#: data/resources/ui_templates/article_view/column.blp:120
#: data/resources/ui_templates/article_view/column.blp:186
msgid "Try to Show Full Content"
msgstr "Försök att visa allt innehåll"

#: data/resources/ui_templates/article_view/column.blp:140
#: data/resources/ui_templates/article_view/column.blp:205
#: data/resources/ui_templates/settings/shortcuts.blp:347
msgid "Tag Article"
msgstr "Tagga artikel"

#: data/resources/ui_templates/article_view/column.blp:153
#: data/resources/ui_templates/article_view/column.blp:217
msgid "Share To"
msgstr "Dela till"

#: data/resources/ui_templates/article_view/url.blp:18
#: data/resources/ui_templates/settings/share.blp:133
msgid "URL"
msgstr "URL"

#: data/resources/ui_templates/article_view/view.blp:35
msgid "crash"
msgstr "krash"

#: data/resources/ui_templates/article_view/view.blp:47
msgid "WebKit Crashed"
msgstr "WebKit krashade"

#: data/resources/ui_templates/article_view/view.blp:65
msgid "empty"
msgstr "tom"

#: data/resources/ui_templates/article_view/view.blp:68
msgid "Select an Article"
msgstr "Välj en artikel"

#: data/resources/ui_templates/article_view/view.blp:69
msgid " "
msgstr " "

#: data/resources/ui_templates/article_view/view.blp:76
msgid "article"
msgstr "artikel"

#: data/resources/ui_templates/discover/dialog.blp:14
#: data/resources/ui_templates/discover/dialog.blp:24
msgid "Discover"
msgstr "Upptäck"

#: data/resources/ui_templates/discover/dialog.blp:25
msgid "Search the feedly.com library"
msgstr "Sök på Feedly.com biblioteket"

#: data/resources/ui_templates/discover/dialog.blp:61
msgid "Search by #topic, Website or RSS Link"
msgstr "Sök efter #topic, webbplats eller RSS URL"

#: data/resources/ui_templates/discover/dialog.blp:82
msgid "page0"
msgstr "sida0"

#: data/resources/ui_templates/discover/dialog.blp:90
msgid "Featured Topics"
msgstr "Utvalda ämnen"

#: data/resources/ui_templates/discover/dialog.blp:111
msgid "#News"
msgstr "#Nyheter"

#: data/resources/ui_templates/discover/dialog.blp:123
msgid "#Tech"
msgstr "#Teknik"

#: data/resources/ui_templates/discover/dialog.blp:133
msgid "#Science"
msgstr "#Forskning"

#: data/resources/ui_templates/discover/dialog.blp:143
msgid "#Culture"
msgstr "#Kultur"

#: data/resources/ui_templates/discover/dialog.blp:155
msgid "#Media"
msgstr "#Media"

#: data/resources/ui_templates/discover/dialog.blp:167
msgid "#Sports"
msgstr "#Sport"

#: data/resources/ui_templates/discover/dialog.blp:179
msgid "#Food"
msgstr "#Mat"

#: data/resources/ui_templates/discover/dialog.blp:191
msgid "#Open source"
msgstr "#Öppen källkod"

#: data/resources/ui_templates/discover/dialog.blp:204
msgid "page1"
msgstr "sida1"

#: data/resources/ui_templates/discover/dialog.blp:214
msgid "Search Results"
msgstr "Sökresultat"

#: data/resources/ui_templates/discover/dialog.blp:278
msgid "No Results"
msgstr "Inga resultat"

#: data/resources/ui_templates/discover/search_item.blp:31
msgid "Title"
msgstr "Titel"

#: data/resources/ui_templates/discover/search_item.blp:42
msgid "Description"
msgstr "Beskrivning"

#: data/resources/ui_templates/edit_dialogs/category.blp:16
msgid "Edit Category"
msgstr "Redigera kategori"

#: data/resources/ui_templates/edit_dialogs/category.blp:40
msgid "Category Name"
msgstr "Kategorinamn"

#: data/resources/ui_templates/edit_dialogs/feed.blp:16
msgid "Edit Feed"
msgstr "Redigera flöde"

#: data/resources/ui_templates/edit_dialogs/feed.blp:40
msgid "Feed Name"
msgstr "Flödets namn"

#: data/resources/ui_templates/edit_dialogs/feed.blp:45
msgid "Feed URL"
msgstr "Flödets URL"

#: data/resources/ui_templates/edit_dialogs/feed.blp:56
msgid "Content"
msgstr "Innehåll"

#: data/resources/ui_templates/edit_dialogs/feed.blp:60
#, fuzzy
msgid "Inline Math delimiter"
msgstr "Inline Math Delimiter"

#: data/resources/ui_templates/edit_dialogs/feed.blp:65
msgid "Scrape Content"
msgstr "Skrapa innehåll"

#: data/resources/ui_templates/edit_dialogs/feed.blp:66
msgid "Try to fetch content for articles after syncing"
msgstr "Försök att hämta innehåll för artiklar efter synkronisering"

#: data/resources/ui_templates/edit_dialogs/tag.blp:13
msgid "Edit Tag"
msgstr "Redigera tagg"

#: data/resources/ui_templates/edit_dialogs/tag.blp:36
msgid "Tag Name"
msgstr "Taggnamn"

#: data/resources/ui_templates/edit_dialogs/tag.blp:41
msgid "Color"
msgstr "Färg"

#: data/resources/ui_templates/enclosures/button.blp:12
#: src/enclosure_button.rs:88 src/enclosure_button.rs:151
msgid "Open Attachment"
msgstr "Öppna bilaga"

#: data/resources/ui_templates/enclosures/button.blp:23
msgid "Show Attachments"
msgstr "Visa bilagor"

#: data/resources/ui_templates/error_detail_dialog.blp:15
msgid "Error Details"
msgstr "Felmeddelande"

#: data/resources/ui_templates/error_detail_dialog.blp:35
msgid "Copy to Clipboard"
msgstr "Kopiera"

#: data/resources/ui_templates/login/custom_api_secret.blp:25
msgid "Custom API Secret"
msgstr "Egen API hemlighet"

#: data/resources/ui_templates/login/custom_api_secret.blp:50
msgid "Client ID"
msgstr "Klient ID"

#: data/resources/ui_templates/login/custom_api_secret.blp:54
msgid "Client Secret"
msgstr "Klient hemlighet"

#: data/resources/ui_templates/login/custom_api_secret.blp:68
msgid "Use Your Own Secret"
msgstr "Använd din egen hemlighet"

#: data/resources/ui_templates/login/custom_api_secret.blp:81
msgid "Use NewsFlash Secret"
msgstr "Använd NewsFlash hemlighet"

#: data/resources/ui_templates/login/password.blp:32
msgid "Headline"
msgstr "Rubrik"

#: data/resources/ui_templates/login/password.blp:51
msgid "Server URL"
msgstr "Server URL"

#: data/resources/ui_templates/login/password.blp:55
msgid "Login with"
msgstr "Logga in med"

#: data/resources/ui_templates/login/password.blp:57
msgid "Username / Password"
msgstr "Användarnamn / lösenord"

#: data/resources/ui_templates/login/password.blp:57
#: data/resources/ui_templates/login/password.blp:70
msgid "Token"
msgstr "Token"

#: data/resources/ui_templates/login/password.blp:62
#: data/resources/ui_templates/login/password.blp:85
msgid "Username"
msgstr "Användarnamn"

#: data/resources/ui_templates/login/password.blp:66
#: data/resources/ui_templates/login/password.blp:89
msgid "Password"
msgstr "Lösenord"

#: data/resources/ui_templates/login/password.blp:82
msgid "HTTP Basic Auth"
msgstr "HTTP grundläggande inloggning"

#: data/resources/ui_templates/login/password.blp:108
#: data/resources/ui_templates/main_window.blp:21
#: data/resources/ui_templates/main_window.blp:28
#: data/resources/ui_templates/main_window.blp:35
msgid "Log In"
msgstr "Logga in"

#: data/resources/ui_templates/login/service_row.blp:5
msgid "Service Name"
msgstr "Servicenamn"

#: data/resources/ui_templates/login/welcome.blp:16
msgid "Add RSS Service"
msgstr "Lägg till RSS tjänst"

#: data/resources/ui_templates/login/welcome.blp:27
msgid "This Device"
msgstr "Denna enhet"

#: data/resources/ui_templates/login/welcome.blp:39
msgid "Sync Account"
msgstr "Synkronisera konto"

#: data/resources/ui_templates/main_window.blp:50
msgid "Reset"
msgstr "Återställ"

#: data/resources/ui_templates/main_window.blp:57
msgid "Video"
msgstr "Video"

#: data/resources/ui_templates/reset_page.blp:15
msgid "Logout?"
msgstr "Logga ut?"

#: data/resources/ui_templates/reset_page.blp:16
msgid "Logging out leads to all local data being lost"
msgstr "Logga ut leder till att all lokala data förloras"

#: data/resources/ui_templates/reset_page.blp:31 src/account_popover.rs:21
msgid "Logout"
msgstr "Logga ut"

#: data/resources/ui_templates/settings/app.blp:6
msgid "App"
msgstr "App"

#: data/resources/ui_templates/settings/app.blp:9
msgid "Application"
msgstr "Program"

#: data/resources/ui_templates/settings/app.blp:12
msgid "Run in Background"
msgstr "Kör i bakgrunden"

#: data/resources/ui_templates/settings/app.blp:13
msgid "Fetch updates while the app is closed"
msgstr "Hämta uppdateringar när appen är stängd"

#: data/resources/ui_templates/settings/app.blp:24
msgid "Autostart"
msgstr "Starta automatiskt"

#: data/resources/ui_templates/settings/app.blp:25
msgid "Start App on login"
msgstr "Starta program vid inloggning"

#: data/resources/ui_templates/settings/app.blp:36
msgid "Sync on Startup"
msgstr "Synkronisera på start"

#: data/resources/ui_templates/settings/app.blp:37
msgid "Fetch updates when the App is launched"
msgstr "Hämta uppdateringar när appen startas"

#: data/resources/ui_templates/settings/app.blp:48
msgid "Sync on Metered Connection"
msgstr "Synkronisera på databegränsad anslutning"

#: data/resources/ui_templates/settings/app.blp:49
msgid "Fetch updates on mobile connections in the background"
msgstr "Hämta uppdateringar på mobila anslutningar i bakgrunden"

#: data/resources/ui_templates/settings/app.blp:61
msgid "Update Interval"
msgstr "Uppdateringsintervall"

#: data/resources/ui_templates/settings/app.blp:64 src/settings/feed_list.rs:32
msgid "Manual"
msgstr "Manuellt"

#: data/resources/ui_templates/settings/app.blp:65
msgid "No automatic synchronization"
msgstr "Ingen automatisk synkronisering"

#: data/resources/ui_templates/settings/app.blp:75
msgid "Sync Every"
msgstr "Synkronisera varje"

#: data/resources/ui_templates/settings/app.blp:86
#: data/resources/ui_templates/settings/share.blp:111
msgid "Custom"
msgstr "Anpassad"

#: data/resources/ui_templates/settings/app.blp:96
msgid "hh:mm:ss"
msgstr "hh:mm:ss"

#: data/resources/ui_templates/settings/app.blp:107
msgid "Data"
msgstr "Data"

#: data/resources/ui_templates/settings/app.blp:110
msgid "User Data"
msgstr "Användardata"

#: data/resources/ui_templates/settings/app.blp:127
msgid "Keep Articles"
msgstr "Behåll artiklar"

#: data/resources/ui_templates/settings/app.blp:128
msgid "Refers to the time of sync"
msgstr "Refererar till synkroniseringstiden"

#: data/resources/ui_templates/settings/app.blp:132
msgid "Database"
msgstr "Databas"

#: data/resources/ui_templates/settings/app.blp:133
msgid "Delete old data &amp; rewrite file"
msgstr "Ta bort gammal data &amp; skriver om filen"

#: data/resources/ui_templates/settings/app.blp:140
msgid "Clean"
msgstr "Rengör"

#: data/resources/ui_templates/settings/app.blp:152
msgid "Cache"
msgstr "Cache"

#: data/resources/ui_templates/settings/app.blp:169
msgid "Webview"
msgstr "Webbläge"

#: data/resources/ui_templates/settings/app.blp:177
msgid "Clear"
msgstr "Töm"

#: data/resources/ui_templates/settings/keybind_editor.blp:34
msgid "Enter New Keybinding"
msgstr ""

#: data/resources/ui_templates/settings/keybind_editor.blp:44
msgid "STUFF"
msgstr ""

#: data/resources/ui_templates/settings/keybind_editor.blp:56
msgid "Press ESC to cancel or Backspace to disable the keybinding"
msgstr ""

#: data/resources/ui_templates/settings/keybind_editor.blp:64
msgid "SHORTCUT"
msgstr ""

#: data/resources/ui_templates/settings/keybind_editor.blp:75
msgid "Set"
msgstr ""

#: data/resources/ui_templates/settings/share.blp:6
msgid "Sharing"
msgstr ""

#: data/resources/ui_templates/settings/share.blp:9
msgid "Services"
msgstr ""

#: data/resources/ui_templates/settings/share.blp:12
msgid "Pocket"
msgstr ""

#: data/resources/ui_templates/settings/share.blp:26
msgid "Instapaper"
msgstr ""

#: data/resources/ui_templates/settings/share.blp:40
msgid "Twitter"
msgstr ""

#: data/resources/ui_templates/settings/share.blp:54
msgid "Mastodon"
msgstr ""

#: data/resources/ui_templates/settings/share.blp:68
msgid "Reddit"
msgstr ""

#: data/resources/ui_templates/settings/share.blp:82
msgid "Telegram"
msgstr ""

#: data/resources/ui_templates/settings/share.blp:96
msgid "Clipboard"
msgstr ""

#: data/resources/ui_templates/settings/share.blp:112
msgid ""
"Add your own share link. ${url} and ${title} will be replaced with the url "
"and title \trespectively."
msgstr ""

#: data/resources/ui_templates/settings/share.blp:115
msgid "Enabled"
msgstr ""

#: data/resources/ui_templates/settings/share.blp:128
msgid "Name"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:6
msgid "Keybindings"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:9
#: data/resources/ui_templates/settings/views.blp:32
msgid "Article List"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:12
msgid "Next Article"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:29
msgid "Previous Article"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:80
msgid "Open URL"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:97
msgid "Copy URL"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:115
#: data/resources/ui_templates/settings/views.blp:9
msgid "Feed List"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:118
msgid "Next Item"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:135
msgid "Previous Item"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:152
msgid "Expand / Collapse"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:169
msgid "Mark Selected Read"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:187
msgid "General"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:190
msgid "Refresh"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:207
msgid "Search"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:224
#: src/content_page/sidebar_column.rs:128
msgid "Quit"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:241
#: data/resources/ui_templates/sidebar/sidebar.blp:31
msgid "All Articles"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:258
msgid "Only Unread"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:275
msgid "Only Starred"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:293
#: data/resources/ui_templates/settings/views.blp:70
msgid "Article View"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:296
msgid "Scroll Up"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:313
msgid "Scroll Down"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:330
msgid "Scrape Article Content"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:364
msgid "Fullscreen Article"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:381
#: src/content_page/article_view_column.rs:227
msgid "Close Article"
msgstr ""

#: data/resources/ui_templates/settings/theme_chooser.blp:28
#: data/resources/ui_templates/settings/views.blp:90
msgid "Default"
msgstr ""

#: data/resources/ui_templates/settings/theme_chooser.blp:42
msgid "Spring"
msgstr ""

#: data/resources/ui_templates/settings/theme_chooser.blp:56
msgid "Midnight"
msgstr ""

#: data/resources/ui_templates/settings/theme_chooser.blp:70
msgid "Parchment"
msgstr ""

#: data/resources/ui_templates/settings/theme_chooser.blp:84
msgid "Gruvbox"
msgstr ""

#: data/resources/ui_templates/settings/theme_chooser.blp:98
msgid "Zorin"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:6
msgid "Views"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:12
#: data/resources/ui_templates/settings/views.blp:35
msgid "Order"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:16
msgid "Only Show Relevant Items"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:17
msgid "Hide feeds and categories without unread/unstarred items"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:39
msgid "Show Thumbnails"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:40
msgid "Only if available"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:54
msgid "Hide Future Articles"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:55
msgid "Hide Articles dated in the Future"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:73
msgid "Theme"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:100
msgid "Content Width"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:101
msgid "Width of the Article in Characters"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:114
msgid "Line Height"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:115
msgid "Line Height in Characters"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:129
msgid "Use System Font"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:142
msgid "Font"
msgstr ""

#: data/resources/ui_templates/sidebar/account_widget.blp:19
msgid "label"
msgstr ""

#: data/resources/ui_templates/sidebar/column.blp:13
msgid "Account"
msgstr ""

#: data/resources/ui_templates/sidebar/column.blp:23
msgid "Main Menu"
msgstr ""

#: data/resources/ui_templates/sidebar/column.blp:29
msgid "Add Feed/Category/Tag"
msgstr ""

#: data/resources/ui_templates/sidebar/sidebar.blp:86
msgid "Subscriptions"
msgstr ""

#: data/resources/ui_templates/sidebar/sidebar.blp:127
msgid "Tags"
msgstr ""

#: data/resources/ui_templates/sidebar/sidebar.blp:169
#: src/sidebar/feed_list/item_row.rs:537 src/sidebar/tag_list/tag_row.rs:252
msgid "Set Read"
msgstr ""

#: data/resources/ui_templates/tagging/popover.blp:30
msgid "Add Tags"
msgstr ""

#: data/resources/ui_templates/tagging/popover.blp:38
msgid "Type to create a new Tag"
msgstr ""

#: data/resources/ui_templates/tagging/popover.blp:59
msgid "Create Tag"
msgstr ""

#: data/resources/ui_templates/tagging/popover.blp:96
msgid "Tag the Article"
msgstr ""

#: data/resources/ui_templates/tagging/popover.blp:110
msgid "Press enter to assign a Tag or ctrl + enter to create a new Tag"
msgstr ""

#: data/resources/ui_templates/tagging/popover.blp:147
msgid "Search and Create Tags"
msgstr ""

#: src/account_popover.rs:34
msgid "Edit Account"
msgstr ""

#: src/app.rs:526
msgid "Failed to sync"
msgstr ""

#: src/app.rs:540
msgid "New Articles"
msgstr ""

#: src/app.rs:543
msgid "There is 1 new article ({} unread)"
msgstr ""

#: src/app.rs:546
msgid "There are {} new articles ({} unread)"
msgstr ""

#: src/app.rs:690
msgid "Failed to mark article read: '{}'"
msgstr ""

#: src/app.rs:737
msgid "Failed to star article: '{}'"
msgstr ""

#: src/app.rs:836 src/app.rs:1014
msgid "Failed to mark all read"
msgstr ""

#: src/app.rs:880
msgid "Failed to mark feed read"
msgstr ""

#: src/app.rs:924
msgid "Failed to mark category read"
msgstr ""

#: src/app.rs:968
msgid "Failed to mark tag read"
msgstr ""

#: src/app.rs:1057
msgid "Failed to mark multiple article ids read"
msgstr ""

#: src/app.rs:1111
msgid "Failed to fetch feed"
msgstr ""

#: src/app.rs:1136
msgid "Failed to add feed"
msgstr ""

#: src/app.rs:1156
msgid "Failed to add category"
msgstr ""

#: src/app.rs:1188
msgid "Failed to add tag"
msgstr ""

#: src/app.rs:1225 src/app.rs:1245
msgid "Failed to rename feed"
msgstr ""

#: src/app.rs:1269
msgid "Failed to rename category"
msgstr ""

#: src/app.rs:1294 src/app.rs:1474
msgid "Failed to tag article"
msgstr ""

#: src/app.rs:1317
msgid "Failed to set feed list order to manual"
msgstr ""

#: src/app.rs:1347
msgid "Failed to move feed"
msgstr ""

#: src/app.rs:1375
msgid "Failed to move category"
msgstr ""

#: src/app.rs:1393
msgid "Failed to delete feed"
msgstr ""

#: src/app.rs:1413
msgid "Failed to delete category"
msgstr ""

#: src/app.rs:1433
msgid "Failed to delete tag"
msgstr ""

#: src/app.rs:1512
msgid "Failed to untag article"
msgstr ""

#: src/app.rs:1584 src/app.rs:1759 src/app.rs:1861
msgid "_Save"
msgstr ""

#: src/app.rs:1585 src/content_page/article_view_column.rs:221
msgid "Export Article"
msgstr ""

#: src/app.rs:1610
msgid "No file set: {}"
msgstr ""

#: src/app.rs:1636
msgid "Failed to download images"
msgstr ""

#. .transient_for(&App::default().main_window())
#: src/app.rs:1668
msgid "_Open"
msgstr ""

#: src/app.rs:1669 src/content_page/sidebar_column.rs:131
msgid "Import OPML"
msgstr ""

#: src/app.rs:1697
msgid "Failed read OPML string: {}"
msgstr ""

#: src/app.rs:1733
msgid "Failed to import OPML"
msgstr ""

#: src/app.rs:1760 src/content_page/sidebar_column.rs:132
msgid "Export OPML"
msgstr ""

#: src/app.rs:1780
msgid "No file set."
msgstr ""

#: src/app.rs:1803
msgid "Failed to get OPML data"
msgstr ""

#: src/app.rs:1814
msgid "Failed to write OPML data to disc"
msgstr ""

#: src/app.rs:1821
msgid "Failed to parse OPML data for formatting"
msgstr ""

#: src/app.rs:1862
msgid "Save Image"
msgstr ""

#: src/app.rs:1882
msgid "No file set"
msgstr ""

#: src/app.rs:1905
msgid "Failed to download image {}"
msgstr ""

#: src/app.rs:2029
msgid "NewsFlash is offline"
msgstr ""

#: src/app.rs:2031
msgid "NewsFlash is online"
msgstr ""

#: src/app.rs:2047
msgid "Failed to set offline"
msgstr ""

#: src/app.rs:2049
msgid "Failed to apply changes made while offline"
msgstr ""

#: src/app.rs:2118
msgid "Failed open URL: {}"
msgstr ""

#: src/edit_feed_dialog.rs:95 src/add_dialog/add_feed_widget.rs:142
msgid "None"
msgstr ""

#: src/enclosure_button.rs:86 src/enclosure_button.rs:149
msgid "Watch Youtube Video"
msgstr ""

#: src/enclosure_button.rs:145
msgid "Open Attached Image"
msgstr ""

#: src/enclosure_button.rs:147
msgid "Open Attached Video"
msgstr ""

#: src/image_dialog.rs:99 src/image_dialog.rs:133
msgid "Failed to decode image '{}'"
msgstr ""

#: src/image_dialog.rs:111
msgid "Failed to download image"
msgstr ""

#: src/image_dialog.rs:143
msgid "Could not decode image"
msgstr ""

#: src/reset_page.rs:92
msgid "Failed to reset account"
msgstr ""

#: src/reset_page.rs:94 src/content_page/mod.rs:146
#: src/login_screen/password_login.rs:331
#: src/login_screen/password_login.rs:342
#: src/login_screen/password_login.rs:353 src/login_screen/web_login.rs:124
msgid "details"
msgstr ""

#: src/add_dialog/add_feed_widget.rs:116
msgid "Failed to add feed: No valid url"
msgstr ""

#: src/add_dialog/parse_feed_widget.rs:120
msgid "Not a valid URL"
msgstr ""

#: src/add_dialog/parse_feed_widget.rs:168
msgid "No Feed found"
msgstr ""

#: src/add_dialog/select_feed_widget.rs:104
msgid "Can't parse Feed"
msgstr ""

#: src/article_list/article_row.rs:427
msgid "Toggle Star"
msgstr ""

#: src/article_list/article_row.rs:428
#: src/content_page/article_view_column.rs:223
msgid "Open in Browser"
msgstr ""

#: src/article_list/mod.rs:652
msgid "No articles that fit \"{}\""
msgstr ""

#: src/article_list/mod.rs:656
msgid "No unread articles that fit \"{}\""
msgstr ""

#: src/article_list/mod.rs:657
msgid "No Unread Articles"
msgstr ""

#: src/article_list/mod.rs:660
msgid "No starred articles that fit \"{}\""
msgstr ""

#: src/article_list/mod.rs:661
msgid "No Starred Articles"
msgstr ""

#: src/article_list/mod.rs:672
msgid "No articles that fit \"{}\" in {} \"{}\""
msgstr ""

#: src/article_list/mod.rs:675
msgid "No articles in {} \"{}\""
msgstr ""

#: src/article_list/mod.rs:679
msgid "No unread articles that fit \"{}\" in {} \"{}\""
msgstr ""

#: src/article_list/mod.rs:682
msgid "No unread articles in {} \"{}\""
msgstr ""

#: src/article_list/mod.rs:686
msgid "No starred articles that fit \"{}\" in {} \"{}\""
msgstr ""

#: src/article_list/mod.rs:689
msgid "No starred articles in {} \"{}\""
msgstr ""

#: src/article_list/mod.rs:695
msgid "No articles that fit \"{}\" in tag \"{}\""
msgstr ""

#: src/article_list/mod.rs:696
msgid "No articles in tag \"{}\""
msgstr ""

#: src/article_list/mod.rs:700
msgid "No unread articles that fit \"{}\" in tag \"{}\""
msgstr ""

#: src/article_list/mod.rs:703
msgid "No unread articles in tag \"{}\""
msgstr ""

#: src/article_list/mod.rs:707
msgid "No starred articles that fit \"{}\" in tag \"{}\""
msgstr ""

#: src/article_list/mod.rs:710
msgid "No starred articles in tag \"{}\""
msgstr ""

#: src/content_page/article_view_column.rs:226
msgid "Toggle Fullscreen"
msgstr ""

#: src/content_page/mod.rs:193
msgid "Deleted Category '{}'"
msgstr ""

#: src/content_page/mod.rs:194
msgid "Deleted Feed '{}'"
msgstr ""

#: src/content_page/mod.rs:195
msgid "Deleted Tag '{}'"
msgstr ""

#: src/content_page/mod.rs:199 src/content_page/mod.rs:270
msgid "undo"
msgstr ""

#: src/content_page/mod.rs:269
msgid "Mark all read"
msgstr ""

#: src/content_page/mod.rs:379
msgid "Failed to set service logo"
msgstr ""

#: src/content_page/mod.rs:533
msgid "Failed to update sidebar: '{}'"
msgstr ""

#: src/content_page/sidebar_column.rs:126
msgid "Keyboard Shortcuts"
msgstr ""

#: src/content_page/sidebar_column.rs:127
msgid "About NewsFlash"
msgstr ""

#: src/content_page/sidebar_column.rs:135
msgid "Discover Feeds"
msgstr ""

#: src/content_page/sidebar_column.rs:146
msgid "Feed"
msgstr ""

#: src/content_page/sidebar_column.rs:148
msgid "Tag"
msgstr ""

#. set headline
#: src/login_screen/password_login.rs:155
msgid "Log into {}"
msgstr ""

#: src/login_screen/password_login.rs:299
msgid "Unauthorized"
msgstr ""

#: src/login_screen/password_login.rs:309
msgid "No valid CA certificate available"
msgstr ""

#: src/login_screen/password_login.rs:313
msgid "ignore future TLS errors"
msgstr ""

#: src/login_screen/password_login.rs:327
msgid "Network Error"
msgstr ""

#: src/login_screen/password_login.rs:338
msgid "Could not log in"
msgstr ""

#: src/login_screen/password_login.rs:349 src/login_screen/web_login.rs:113
msgid "Unknown error"
msgstr ""

#: src/login_screen/web_login.rs:111
msgid "API Limit Reached"
msgstr ""

#: src/login_screen/web_login.rs:112
msgid "Failed to log in"
msgstr ""

#: src/settings/dialog/app_page.rs:203 src/settings/dialog/app_page.rs:213
msgid "Failed to set setting 'keep running'"
msgstr ""

#: src/settings/dialog/app_page.rs:220
msgid "Failed to set setting 'sync on startup'"
msgstr ""

#: src/settings/dialog/app_page.rs:227
msgid "Failed to set setting 'sync on metered'"
msgstr ""

#: src/settings/dialog/app_page.rs:291
msgid "Failed to set setting 'limit articles duration'"
msgstr ""

#: src/settings/dialog/app_page.rs:440
msgid "Failed to set setting 'sync interval'"
msgstr ""

#: src/settings/dialog/keybinding_editor.rs:108
msgid "Disable Keybinding"
msgstr ""

#: src/settings/dialog/keybinding_editor.rs:125
msgid "Illegal Keybinding"
msgstr ""

#: src/settings/dialog/share_page.rs:115
msgid "Failed to set setting 'pocket enabled'"
msgstr ""

#: src/settings/dialog/share_page.rs:133
msgid "Failed to set setting 'instapaper enabled'"
msgstr ""

#: src/settings/dialog/share_page.rs:151
msgid "Failed to set setting 'twitter enabled'"
msgstr ""

#: src/settings/dialog/share_page.rs:169
msgid "Failed to set setting 'mastodon enabled'"
msgstr ""

#: src/settings/dialog/share_page.rs:187
msgid "Failed to set setting 'reddit enabled'"
msgstr ""

#: src/settings/dialog/share_page.rs:205
msgid "Failed to set setting 'telegram enabled'"
msgstr ""

#: src/settings/dialog/share_page.rs:223
msgid "Failed to set setting 'clipboard enabled'"
msgstr ""

#: src/settings/dialog/share_page.rs:247
msgid "Failed to set setting 'custom share enabled'"
msgstr ""

#: src/settings/dialog/share_page.rs:265
msgid "Failed to set setting 'custom share name'"
msgstr ""

#: src/settings/dialog/share_page.rs:282
msgid "Failed to set setting 'custom share url'"
msgstr ""

#: src/settings/dialog/shortcuts_page.rs:367
#: src/settings/dialog/shortcuts_page.rs:375
msgid "Failed to write keybinding"
msgstr ""

#: src/settings/dialog/views_page.rs:131
msgid "Newest First"
msgstr ""

#: src/settings/dialog/views_page.rs:132
msgid "Oldest First"
msgstr ""

#: src/settings/dialog/views_page.rs:228
msgid "Failed to set setting 'show relevant feeds'"
msgstr ""

#: src/settings/dialog/views_page.rs:249
msgid "Failed to set setting 'article order'"
msgstr ""

#: src/settings/dialog/views_page.rs:262 src/settings/dialog/views_page.rs:275
msgid "Failed to set setting 'show thumbnails'"
msgstr ""

#: src/settings/dialog/views_page.rs:308
msgid "Failed to set article theme"
msgstr ""

#: src/settings/dialog/views_page.rs:334
msgid "Failed to set setting 'content width'"
msgstr ""

#: src/settings/dialog/views_page.rs:352
msgid "Failed to set setting 'line height'"
msgstr ""

#: src/settings/dialog/views_page.rs:366
msgid "Failed to set setting 'article font'"
msgstr ""

#: src/settings/dialog/views_page.rs:388
msgid "Failed to set setting 'use system font'"
msgstr ""

#: src/settings/feed_list.rs:31
msgid "Alphabetical"
msgstr ""

#: src/settings/general.rs:45
msgid "15 Minutes"
msgstr ""

#: src/settings/general.rs:46
msgid "30 Minutes"
msgstr ""

#: src/settings/general.rs:47
msgid "1 Hour"
msgstr ""

#: src/settings/general.rs:48
msgid "2 Hours"
msgstr ""

#: src/settings/general.rs:101
msgid "Forever"
msgstr ""

#: src/settings/general.rs:102
msgid "One Year"
msgstr ""

#: src/settings/general.rs:103
msgid "6 Months"
msgstr ""

#: src/settings/general.rs:104
msgid "One Month"
msgstr ""

#: src/settings/general.rs:105
msgid "One Week"
msgstr ""

#: src/share/share_popover.rs:155
msgid "No article selected"
msgstr ""

#: src/share/share_popover.rs:162
msgid "Article does not have URL"
msgstr ""

#: src/share/share_popover.rs:170
msgid "Article shared with {}"
msgstr ""

#: src/sidebar/feed_list/item_row.rs:538 src/sidebar/tag_list/tag_row.rs:253
msgid "Edit"
msgstr ""

#: src/sidebar/feed_list/item_row.rs:539 src/sidebar/tag_list/tag_row.rs:254
msgid "Delete"
msgstr ""

#: src/util/date_util.rs:28
msgid "Today"
msgstr ""

#: src/util/date_util.rs:30
msgid "Yesterday"
msgstr ""
